// Copyright (C) 2021  The NeoAnt Project Developers
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![no_std]

// -------------------------------------------------------------------
//
// Book: https://doc.rust-lang.org/rustc/lints/index.html
//
// # Lint Groups
//
#![forbid(future_incompatible)]
#![forbid(nonstandard_style)]
#![forbid(rust_2018_compatibility)]
#![forbid(rust_2018_idioms)]
#![forbid(unused)]
//
// # Deny by Default Lints
//
#![forbid(arithmetic_overflow)]
#![forbid(incomplete_include)]
#![forbid(ineffective_unstable_trait_impl)]
#![forbid(mutable_transmutes)]
#![forbid(no_mangle_const_items)]
#![forbid(overflowing_literals)]
#![forbid(unconditional_panic)]
#![forbid(unknown_crate_types)]
#![forbid(useless_deprecated)]
//
// # Warn by Default Lints
//
#![forbid(asm_sub_register)]
#![forbid(bindings_with_variant_name)]
#![forbid(clashing_extern_declarations)]
#![forbid(confusable_idents)]
#![forbid(const_item_mutation)]
#![forbid(deprecated)]
#![forbid(drop_bounds)]
#![forbid(exported_private_dependencies)]
#![forbid(function_item_references)]
#![forbid(improper_ctypes)]
#![forbid(improper_ctypes_definitions)]
#![forbid(incomplete_features)]
#![forbid(inline_no_sanitize)]
#![forbid(invalid_value)]
#![forbid(irrefutable_let_patterns)]
#![forbid(mixed_script_confusables)]
#![forbid(no_mangle_generic_items)]
#![forbid(non_fmt_panic)]
#![forbid(non_shorthand_field_patterns)]
#![forbid(overlapping_range_endpoints)]
#![forbid(renamed_and_removed_lints)]
#![forbid(stable_features)]
#![forbid(temporary_cstring_as_ptr)]
#![forbid(trivial_bounds)]
#![forbid(type_alias_bounds)]
#![forbid(uncommon_codepoints)]
#![forbid(unconditional_recursion)]
#![forbid(unknown_lints)]
#![forbid(unnameable_test_items)]
#![forbid(unused_comparisons)]
#![forbid(while_true)]
//
// # Allowed by Default Lints
//
#![forbid(macro_use_extern_crate)]
#![forbid(meta_variable_misuse)]
#![forbid(missing_abi)]
#![forbid(missing_copy_implementations)]
#![warn(missing_debug_implementations)]
#![warn(missing_docs)]
#![forbid(non_ascii_idents)]
#![forbid(noop_method_call)]
#![forbid(single_use_lifetimes)]
#![forbid(trivial_casts)]
#![forbid(trivial_numeric_casts)]
#![forbid(unaligned_references)]
#![forbid(unreachable_pub)]
#![forbid(unsafe_code)]
#![forbid(unsafe_op_in_unsafe_fn)]
#![forbid(unused_crate_dependencies)]
#![forbid(unused_import_braces)]
#![forbid(unused_lifetimes)]
#![forbid(unused_results)]
#![forbid(variant_size_differences)]
#![deny(unused_qualifications)]
//
// # Clippy lints
//
#![forbid(clippy::correctness)]
#![forbid(clippy::style)]
#![forbid(clippy::complexity)]
#![forbid(clippy::perf)]
#![forbid(clippy::pedantic)]
#![forbid(clippy::nursery)]
#![forbid(clippy::cargo)]
// -------------------------------------------------------------------


//! [UEFI Specification Version 2.8 (released March 2019)][1]
//!
//! [1]: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_8_final.pdf

/// # EFI System Table

/// EFI_TABLE_HEADER
///
/// The data type EFI_TABLE_HEADER is the data structure that precedes all of
/// the standard EFI table types. It includes a signature that is unique for
/// each table type, a revision of the table that may be updated as extensions
/// are added to the EFI table types, and a 32-bit CRC so a consumer of an EFI
/// table type can validate the contents of the EFI table.
///
/// # Summary
///
/// Data structure that precedes all of the standard EFI table types.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TableHeader {
    /// A 64-bit signature that identifies the type of table that follows.
    /// Unique signatures have been generated for the EFI System Table, the
    /// EFI Boot Services Table, and the EFI Runtime Services Table.
    pub signature: u64,

    /// The revision of the EFI Specification to which this table conforms.
    /// The upper 16 bits of this field contain the major revision value, and
    /// the lower 16 bits contain the minor revision value. The minor revision
    /// values are binary coded decimals and are limited to the range of 00..99.
    pub revision: u32,

    /// The size, in bytes, of the entire table including the EFI_TABLE_HEADER.
    pub header_size: u32,

    /// The 32-bit CRC for the entire table. This value is computed by setting
    /// this field to 0, and computing the 32-bit CRC for HeaderSize bytes.
    pub crc32: u32,

    /// Reserved field that must be set to 0.
    reserved: u32,
}

/// EFI_SYSTEM_TABLE
///
/// UEFI uses the EFI System Table, which contains pointers to the runtime and
/// boot services tables. Prior to a call to
/// EFI_BOOT_SERVICES.ExitBootServices(), all of the fields of the EFI System
/// Table are valid. After an operating system has taken control of the platform
/// with a call to ExitBootServices(), only the Hdr, FirmwareVendor,
/// FirmwareRevision, RuntimeServices, NumberOfTableEntries, and
/// ConfigurationTable fields are valid.
///
/// # Summary
///
/// Contains pointers to the runtime and boot services tables.
#[repr(C)]
pub struct SystemTable<'a> {
    /// The table header for the EFI System Table. This header contains the
    /// EFI_SYSTEM_TABLE_SIGNATURE and EFI_SYSTEM_TABLE_REVISION values along
    /// with the size of the EFI_SYSTEM_TABLE structure and a 32-bit CRC to
    /// verify that the contents of the EFI System Table are valid.
    pub hdr: TableHeader,

    /// A pointer to a null terminated string that identifies the vendor that
    /// produces the system firmware for the platform.
    pub firmware_vendor: &'a u16,

    /// A firmware vendor specific value that identifies the revision of the
    /// system firmware for the platform.
    pub firmware_revision: u32,

    /// The handle for the active console input device. This handle must support
    /// EFI_SIMPLE_TEXT_INPUT_PROTOCOL and EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL.
    pub console_in_handle: Handle,

    /// A pointer to the EFI_SIMPLE_TEXT_INPUT_PROTOCOL interface that is
    /// associated with ConsoleInHandle.
    pub con_in: &'a SimpleTextInputProtocol,

    /// The handle for the active console output device. This handle must
    /// support the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.
    pub console_out_handle: Handle,

    /// A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL interface that is
    /// associated with ConsoleOutHandle.
    pub con_out: &'a SimpleTextOutputProtocol<'a>,

    /// The handle for the active standard error console device. This handle
    /// must support the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.
    pub standard_error_handle: Handle,

    /// A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL interface that is
    /// associated with StandardErrorHandle.
    pub std_err: &'a SimpleTextOutputProtocol<'a>,

    /// A pointer to the EFI Runtime Services Table.
    pub runtime_services: &'a RuntimeServices,

    /// A pointer to the EFI Boot Services Table.
    pub boot_services: &'a BootServices,

    /// The number of system configuration tables in the buffer
    /// ConfigurationTable.
    pub number_of_table_entries: usize,

    /// A pointer to the system configuration tables. The number of entries in
    /// the table is NumberOfTableEntries.
    pub configuration_table: &'a ConfigurationTable,
}

/// EFI_BOOT_SERVICES
///
/// UEFI uses the EFI Boot Services Table, which contains a table header and
/// pointers to all of the boot services. Except for the table header, all
/// elements in the EFI Boot Services Tables are prototypes of function pointers
/// to functions. The function pointers in this table are not valid after the
/// operating system has taken control of the platform with a call to
/// EFI_BOOT_SERVICES.ExitBootServices().
///
/// # Summary
///
/// Contains a table header and pointers to all of the boot services.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct BootServices {
    /// The table header for the EFI Boot Services Table. This header contains
    /// the EFI_BOOT_SERVICES_SIGNATURE and EFI_BOOT_SERVICES_REVISION values
    /// along with the size of the EFI_BOOT_SERVICES structure and a 32-bit CRC
    /// to verify that the contents of the EFI Boot Services Table are valid.
    pub hdr: TableHeader,

    /// Raises the task priority level.
    pub raise_tpl: RaiseTpl,

    /// Restores/lowers the task priority level.
    pub restore_tpl: RestoreTpl,

    /// Allocates pages of a particular type.
    pub allocate_pages: AllocatePages,

    /// Frees allocated pages.
    pub free_pages: FreePages,

    /// Returns the current boot services memory map and memory map key.
    pub get_memory_map: GetMemoryMap,

    /// Allocates a pool of a particular type.
    pub allocate_pool: AllocatePool,

    /// Frees allocated pool.
    pub free_pool: FreePool,

    /// Creates a general-purpose event structure.
    pub create_event: CreateEvent,

    /// Sets an event to be signaled at a particular time.
    pub set_timer: SetTimer,

    /// Stops execution until an event is signaled.
    pub wait_for_event: WaitForEvent,

    /// Signals an event.
    pub signal_event: SignalEvent,

    /// Closes and frees an event structure.
    pub close_event: CloseEvent,

    /// Checks whether an event is in the signaled state.
    pub check_event: CheckEvent,

    /// Installs a protocol interface on a device handle.
    pub install_protocol_interface: InstallProtocolInterface,

    /// Reinstalls a protocol interface on a device handle.
    pub reinstall_protocol_interface: ReinstallProtocolInterface,

    /// Removes a protocol interface from a device handle.
    pub uninstall_protocol_interface: UninstallProtocolInterface,

    /// Queries a handle to determine if it supports a specified protocol.
    pub handle_protocol: HandleProtocol,

    /// Reserved. Must be NULL.
    reserved: *mut core::ffi::c_void,

    /// Registers an event that is to be signaled whenever an interface is
    /// installed for a specified protocol.
    pub register_protocol_notify: RegisterProtocolNotify,

    /// Returns an array of handles that support a specified protocol.
    pub locate_handle: LocateHandle,

    /// Locates all devices on a device path that support a specified protocol
    /// and returns the handle to the device that is closest to the path.
    pub locate_device_path: LocateDevicePath,

    /// Adds, updates, or removes a configuration table from the EFI System
    /// Table.
    pub install_configuration_table: InstallConfigurationTable,

    /// Loads an EFI image into memory.
    pub load_image: ImageLoad,

    /// Transfers control to a loaded image’s entry point.
    pub start_image: ImageStart,

    /// Exits the image’s entry point.
    pub exit: Exit,

    /// Unloads an image.
    pub unload_image: ImageUnload,

    /// Terminates boot services.
    pub exit_boot_services: ExitBootServices,

    /// Returns a monotonically increasing count for the platform.
    pub get_next_monotonic_count: GetNextMonotonicCount,

    /// Stalls the processor.
    pub stall: Stall,

    /// Resets and sets a watchdog timer used during boot services time.
    pub set_watchdog_timer: SetWatchdogTimer,

    /// Uses a set of precedence rules to find the best set of drivers to manage
    /// a controller.
    pub connect_controller: ConnectController,

    /// Informs a set of drivers to stop managing a controller.
    pub disconnect_controller: DisconnectController,

    /// Adds elements to the list of agents consuming a protocol interface.
    pub open_protocol: OpenProtocol,

    /// Removes elements from the list of agents consuming a protocol interface.
    pub close_protocol: CloseProtocol,

    /// Retrieve the list of agents that are currently consuming a protocol
    /// interface.
    pub open_protocol_information: OpenProtocolInformation,

    /// Retrieves the list of protocols installed on a handle. The return buffer
    /// is automatically allocated.
    pub protocols_per_handle: ProtocolsPerHandle,

    /// Retrieves the list of handles from the handle database that meet the
    /// search criteria. The return buffer is automatically allocated.
    pub locate_handle_buffer: LocateHandleBuffer,

    /// Finds the first handle in the handle database the supports the requested
    /// protocol.
    pub locate_protocol: LocateProtocol,

    /// Installs one or more protocol interfaces onto a handle.
    pub install_multiple_protocol_interfaces: InstallMultipleProtocolInterfaces,

    /// Uninstalls one or more protocol interfaces from a handle.
    pub uninstall_multiple_protocol_interfaces: UninstallMultipleProtocolInterfaces,

    /// Computes and returns a 32-bit CRC for a data buffer.
    pub calculate_crc32: CalculateCrc32,

    /// Copies the contents of one buffer to another buffer.
    pub copy_mem: CopyMem,

    /// Fills a buffer with a specified value.
    pub set_mem: SetMem,

    /// Creates an event structure as part of an event group.
    pub create_event_ex: CreateEventEx,
}

/// EFI_RUNTIME_SERVICES
///
/// UEFI uses the EFI Runtime Services Table, which contains a table header and
/// pointers to all of the runtime services. Except for the table header, all
/// elements in the EFI Runtime Services Tables are prototypes of function
/// pointers to functions. Unlike the EFI Boot Services Table, this table, and
/// the function pointers it contains are valid after the UEFI OS loader and OS
/// have taken control of the platform with a call to
/// EFI_BOOT_SERVICES.ExitBootServices(). If a call to SetVirtualAddressMap() is
/// made by the OS, then the function pointers in this table are fixed up to
/// point to the new virtually mapped entry points.
///
/// # Summary
///
/// Contains a table header and pointers to all of the runtime services.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct RuntimeServices {
    /// The table header for the EFI Runtime Services Table. This header
    /// contains the EFI_RUNTIME_SERVICES_SIGNATURE and
    /// EFI_RUNTIME_SERVICES_REVISION values along with the size of the
    /// EFI_RUNTIME_SERVICES structure and a 32-bit CRC to verify that the
    /// contents of the EFI Runtime Services Table are valid.
    pub hdr: TableHeader,

    /// Returns the current time and date, and the time-keeping capabilities of
    /// the platform.
    pub get_time: GetTime,

    /// Sets the current local time and date information.
    pub set_time: SetTime,

    /// Returns the current wakeup alarm clock setting.
    pub get_wakeup_time: GetWakeupTime,

    /// Sets the system wakeup alarm clock time.
    pub set_wakeup_time: SetWakeupTime,

    /// Used by a UEFI OS loader to convert from physical addressing tovirtual
    /// addressing.
    pub set_virtual_address_map: SetVirtualAddressMap,

    /// Used by EFI components to convert internal pointers when switching to
    /// virtual addressing.
    pub convert_pointer: ConvertPointer,

    /// Returns the value of a variable.
    pub get_variable: GetVariable,

    /// Enumerates the current variable names.
    pub get_next_variable_name: GetNextVariableName,

    /// Sets the value of a variable.
    pub set_variable: SetVariable,

    /// Returns the next high 32 bits of the platform's monotonic counter.
    pub get_next_high_monotonic_count: GetNextHighMonotonicCount,

    /// Resets the entire platform.
    pub reset_system: ResetSystem,

    /// Passes capsules to the firmware with both virtual and physical mapping.
    pub update_capsule: UpdateCapsule,

    /// Returns if the capsule can be supported via UpdateCapsule().
    pub query_capsule_capabilities: QueryCapsuleCapabilities,

    /// Returns information about the EFI variable store.
    pub query_variable_info: QueryVariableInfo,
}

/// EFI_CONFIGURATION_TABLE
///
/// The EFI Configuration Table is the ConfigurationTable field in the EFI
/// System Table. This table contains a set of GUID/pointer pairs. The number of
/// types of configuration tables is expected to grow over time. This is why a
/// GUID is used to identify the configuration table type. The EFI Configuration
/// Table may contain at most once instance of each table type.
///
/// # Summary
///
/// Contains a set of GUID/pointer pairs comprised of the ConfigurationTable
/// field in the EFI System Table.
#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct ConfigurationTable {
    /// The 128-bit GUID value that uniquely identifies the system configuration
    /// table.
    pub vendor_guid: Guid,

    /// A pointer to the table associated with VendorGuid.Whether this pointer
    /// is a physical address or a virtual address during runtime is determined
    /// by the VendorGuid. The VendorGuid associated with a given VendorTable
    /// pointer defines whether or not a particular address reported in the
    /// table gets fixed up when a call to SetVirtualAddressMap() is made. It is
    /// the responsibility of the specification defining the VendorTable to
    /// specify whether to convert the addresses reported in the table.
    pub vendor_table: *mut core::ffi::c_void,
}

/// # Services - Boot Services

/// Creates an event.
///
/// # Parameters
///
/// * Type - The type of event to create and its mode and attributes.
/// * NotifyTpl - The task priority level of event notifications, if needed. See
///   EFI_BOOT_SERVICES.RaiseTPL().
/// * NotifyFunction - Pointer to the event’s notification function, if any.
/// * NotifyContext - Pointer to the notification function’s context;
///   corresponds to parameter Context in the notification function.
/// * Event - Pointer to the newly created event if the call succeeds; undefined
///   otherwise.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event structure was created.
/// * EFI_INVALID_PARAMETER - One of the parameters has an invalid value.
/// * EFI_INVALID_PARAMETER - Event is NULL.
/// * EFI_INVALID_PARAMETER - Type has an unsupported bit set.
/// * EFI_INVALID_PARAMETER - Type has both EVT_NOTIFY_SIGNAL and
///   EVT_NOTIFY_WAIT set.
/// * EFI_INVALID_PARAMETER - Type has either EVT_NOTIFY_SIGNAL or
///   EVT_NOTIFY_WAIT set and NotifyFunction is NULL.
/// * EFI_INVALID_PARAMETER - Type has either EVT_NOTIFY_SIGNAL or
///   EVT_NOTIFY_WAIT set and NotifyTpl is not a supported TPL level.
/// * EFI_OUT_OF_RESOURCES - The event could not be allocated.
pub type CreateEvent =
    extern "C" fn(u32, Tpl, EventNotify, *mut core::ffi::c_void, &mut Event) -> Status;

// EFI_CREATE_EVENT Related Definitions

/// Handle to an event structure.
#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
pub struct Event(pub *mut core::ffi::c_void);

/// EFI_EVENT_NOTIFY
///
/// # Parameters
///
/// * Event - Event whose notification function is being invoked.
/// * Context - Pointer to the notification function’s context, which is
///   implementation-dependent. Context corresponds to NotifyContext in
///   EFI_BOOT_SERVICES.CreateEventEx().
pub type EventNotify = extern "C" fn(Event, *mut core::ffi::c_void);

/// Creates an event in a group.
///
/// # Parameters
///
/// * Type - The type of event to create and its mode and attributes.
/// * NotifyTpl - The task priority level of event notifications,if needed. See
///   EFI_BOOT_SERVICES.RaiseTPL().
/// * NotifyFunction - Pointer to the event’s notification function, if any.
/// * NotifyContext - Pointer to the notification function’s context;
///   corresponds to parameter Context in the notification function.
/// * EventGroup - Pointer to the unique identifier of the group to which this
///   event belongs. If this is NULL, then the function behaves as if the
///   parameters were passed to CreateEvent.
/// * Event - Pointer to the newly created event if the call succeeds; undefined
///   otherwise.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event structure was created.
/// * EFI_INVALID_PARAMETER - One of the parameters has an invalid value.
/// * EFI_INVALID_PARAMETER - Event is NULL.
/// * EFI_INVALID_PARAMETER - Type has an unsupported bit set.
/// * EFI_INVALID_PARAMETER - Type has both EVT_NOTIFY_SIGNAL and
///   EVT_NOTIFY_WAIT set.
/// * EFI_INVALID_PARAMETER - Type has either EVT_NOTIFY_SIGNAL or
///   EVT_NOTIFY_WAIT set and NotifyFunction is NULL.
/// * EFI_INVALID_PARAMETER - Type has either EVT_NOTIFY_SIGNAL or
///   EVT_NOTIFY_WAIT set and NotifyTpl is not a supported TPL level.
/// * EFI_OUT_OF_RESOURCES - The event could not be allocated.
pub type CreateEventEx =
    extern "C" fn(u32, Tpl, EventNotify, *mut core::ffi::c_void, &Guid, &mut Event) -> Status;

/// The CloseEvent() function removes the caller’s reference to the event,
/// removes it from any event group to which it belongs,and closes it. Once the
/// event is closed, the event is no longer valid and may not be used on any
/// subsequent function calls. If Event was registered with
/// RegisterProtocolNotify()then CloseEvent() will remove the corresponding
/// registration. It is safe to callCloseEvent() within the corresponding notify
/// function.
///
/// # Parameters
///
/// * Event - The event to close. Type EFI_EVENTis defined in the CreateEvent()
///   function description.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event has been closed.
pub type CloseEvent = extern "C" fn(Event) -> Status;

/// Signals an event.
///
/// # Parameters
///
/// * Event - The event to signal.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event was signaled.
pub type SignalEvent = extern "C" fn(Event) -> Status;

/// Stops execution until an event is signaled.
///
/// # Parameters
///
/// * NumberOfEvents - The number of events in the Event array.
/// * Event - An array of EFI_EVENT.
/// * Index - Pointer to the index of the event which satisfied the wait
///   condition.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event indicated by Index was signaled.
/// * EFI_INVALID_PARAMETER - NumberOfEvents is 0.
/// * EFI_INVALID_PARAMETER - The event indicated by Index is of type
///   EVT_NOTIFY_SIGNAL.
pub type WaitForEvent = extern "C" fn(usize, &Event, &mut usize) -> Status;

/// Checks whether an event is in the signaled state.
///
/// # Parameters
///
/// * Event - The event to check.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event is in the signaled state.
/// * EFI_NOT_READY - The event is not in the signaled state.
/// * EFI_INVALID_PARAMETER - Event is of type EVT_NOTIFY_SIGNAL.
pub type CheckEvent = extern "C" fn(Event) -> Status;

/// Sets the type of timer and the trigger time for a timer event.
///
/// # Parameters
///
/// * Event - The timer event that is to be signaled at the specified time.
/// * Type - The type of time that is specified in TriggerTime.
/// * TriggerTime - umber of 100ns units until the timer expires. A
///   TriggerTimeof 0 is legal. If Type is TimerRelative and TriggerTime is 0,
///   then the timer event will be signaled on the next timer tick. If Typeis
///   TimerPeriodic and TriggerTime is 0, then the timer event will be signaled
///   on every timer tick.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The event has been set to be signaled at the requested time.
/// * EFI_INVALID_PARAMETER - Event or Type is not valid.
pub type SetTimer = extern "C" fn(Event, TimerDelay, u64) -> Status;

// EFI_SET_TIMER Related Definitions
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum TimerDelay {
    /// The event’s timer setting is to be cancelled and no timer trigger is to
    /// be set. TriggerTime is ignored when canceling a timer.
    TimerCancel,

    /// The event is to be signaled periodically at TriggerTime intervals from
    /// the current time. This is the only timer trigger Type for which the
    /// event timer does not need to be reset for each notification. All other
    /// timer trigger types are "one shot."
    TimerPeriodic,

    /// The event is to be signaled inTriggerTime 100ns units.
    TimerRelative,
}

/// Raises a task's priority level and returns its previous level.
///
/// # Parameters
///
/// * NewTpl - The new task priority level. It must be greater than or equal to
///   the current task priority level.
///
/// # Status Codes Returned
///
/// * Unlike other UEFI interface functions, EFI_BOOT_SERVICES.RaiseTPL() does
///   not return a status code. Instead, it returns the previous task priority
///   level, which is to be restored later with a matching call to RestoreTPL().
pub type RaiseTpl = extern "C" fn(Tpl) -> Tpl;

// EFI_RAISE_TPL Related Definitions

/// Task priority level.
#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
pub struct Tpl(pub usize);

/// The RestoreTPL() function restores a task’s priority level to its previous
/// value. Calls to RestoreTPL()are matched with calls to RaiseTPL().
///
/// # Parameters
///
/// * OldTpl - The previous task priority level to restore (the value from a
///   previous, matching call to EFI_BOOT_SERVICES.RaiseTPL()).
pub type RestoreTpl = extern "C" fn(Tpl);

/// Allocates memory pages from the system.
///
/// # Parameters
///
/// * Type - The type of allocation to perform.
/// * MemoryType - The type of memory to allocate. These memory types are also
///   described in more detail in Table29 and Table30. Normal allocations (that
///   is, allocations by any UEFI application) are of type EfiLoaderData.
///   MemoryType values in the range 0x70000000..0x7FFFFFFF are reserved for OEM
///   use. MemoryTypevalues in the range 0x80000000..0xFFFFFFFF are reserved for
///   use by UEFI OS loaders that are provided by operating system vendors.
/// * Pages - The number of contiguous 4KiB pages to allocate.
/// * Memory - Pointer to a physical address. On input, the way in which the
///   address is used depends on the value of Type. On output the address is set
///   to the base of the page range that was allocated.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The requested pages were allocated.
/// * EFI_OUT_OF_RESOURCES - The pages could not be allocated.
/// * EFI_INVALID_PARAMETER - Type is not AllocateAnyPages or AllocateMaxAddress
///   or AllocateAddress.
/// * EFI_INVALID_PARAMETER - MemoryType is in the range
///   EfiMaxMemoryType..0x6FFFFFFF.
/// * EFI_INVALID_PARAMETER - MemoryType is EfiPersistentMemory.
/// * EFI_INVALID_PARAMETER - Memory is NULL.
/// * EFI_NOT_FOUND - The requested pages could not be found.
pub type AllocatePages =
    extern "C" fn(AllocateType, MemoryType, usize, &mut PhysicalAddress) -> Status;

// EFI_ALLOCATE_PAGES Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum AllocateType {
    /// Allocation requests of TypeAllocateAnyPages allocate any available range
    /// of pages that satisfies the request. On input, the address pointed to by
    /// Memory is ignored.
    AllocateAnyPages,

    /// Allocation requests of TypeAllocateMaxAddressallocate any available
    /// range of pages whose uppermost address is less than or equal to the
    /// address pointed to by Memory on input.
    AllocateMaxAddress,

    /// Allocation requests of TypeAllocateAddress allocate pages at the address
    /// pointed to by Memory on input.
    AllocateAddress,

    MaxAllocateType,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum MemoryType {
    /// # before ExitBootServices()
    ///
    /// Not usable.
    ///
    /// # after ExitBootServices()
    ///
    /// Not usable.
    ReservedMemoryType,

    /// # before ExitBootServices()
    ///
    /// The code portions of a loaded UEFI application.
    ///
    /// # after ExitBootServices()
    ///
    /// The UEFI OS Loader and/or OS may use this memory as they see fit. Note:
    /// the UEFI OS loader that called EFI_BOOT_SERVICES.ExitBootServices() is
    /// utilizing one or more EfiLoaderCode ranges.
    LoaderCode,

    /// # before ExitBootServices()
    ///
    /// The data portions of a loaded UEFI application and the default data
    /// allocation type used by a UEFI application to allocate pool memory.
    ///
    /// # after ExitBootServices()
    ///
    /// The Loader and/or OS may use this memory as they see fit. Note: the OS
    /// loader that called ExitBootServices() is utilizing one or more
    /// EfiLoaderData ranges.
    LoaderData,

    /// # before ExitBootServices()
    ///
    /// The code portions of a loaded UEFI Boot Service Driver.
    ///
    /// # after ExitBootServices()
    ///
    /// Memory available for general use.
    BootServicesCode,

    /// # before ExitBootServices()
    ///
    /// The data portions of a loaded UEFI Boot Serve Driver, and the default
    /// data allocation type used by a UEFI Boot Service Driver to allocate pool
    /// memory.
    ///
    /// # after ExitBootServices()
    ///
    /// Memory available for general use.
    BootServicesData,

    /// # before ExitBootServices()
    ///
    /// The code portions of a loaded UEFI Runtime Driver.
    ///
    /// # after ExitBootServices()
    ///
    /// The memory in this range is to be preserved by the UEFI OS loader and OS
    /// in the working and ACPI S1–S3 states.
    RuntimeServicesCode,

    /// # before ExitBootServices()
    ///
    /// The data portions of a loaded UEFI Runtime Driver and the default data
    /// allocation type used by a UEFI Runtime Driver to allocate pool memory.
    ///
    /// # after ExitBootServices()
    ///
    /// The memory in this range is to be preserved by the UEFI OS l loader and
    /// OS in the working and ACPI S1–S3 states.
    RuntimeServicesData,

    /// # before ExitBootServices()
    ///
    /// Free (unallocated) memory.
    ///
    /// # after ExitBootServices()
    ///
    /// Memory available for general use.
    ConventionalMemory,

    /// # before ExitBootServices()
    ///
    /// Memory in which errors have been detected.
    ///
    /// # after ExitBootServices()
    ///
    /// Memory that contains errors and is not to be used.
    UnusableMemory,

    /// # before ExitBootServices()
    ///
    /// Memory that holds the ACPI tables.
    ///
    /// # after ExitBootServices()
    ///
    /// This memory is to be preserved by the UEFI OS loader and OS until ACPI
    /// is enabled. Once ACPI is enabled, the memory in this range is available
    /// for general use.
    ACPIReclaimMemory,

    /// # before ExitBootServices()
    ///
    /// Address space reserved for use by the firmware.
    ///
    /// # after ExitBootServices()
    ///
    /// This memory is to be preserved by the UEFI OS loader and OS in the
    /// working and ACPI S1–S3 states.
    ACPIMemoryNVS,

    /// # before ExitBootServices()
    ///
    /// Used by system firmware to request that a memory-mapped IO region be
    /// mapped by the OS to a virtual address so it can be accessed by EFI
    /// runtime services.
    ///
    /// # after ExitBootServices()
    ///
    /// This memory is not used by the OS. All system memory-mapped IO
    /// information should come from ACPI tables.
    MemoryMappedIO,

    /// # before ExitBootServices()
    ///
    /// System memory-mapped IO region that is used to translate memory cycles
    /// to IO cycles by the processor.
    ///
    /// # after ExitBootServices()
    ///
    /// This memory is not used by the OS. All system memory-mapped IO port
    /// space information should come from ACPI tables.
    MemoryMappedIOPortSpace,

    /// # before ExitBootServices()
    ///
    /// Address space reserved by the firmware for code that is part of the
    /// processor.
    ///
    /// # after ExitBootServices()
    ///
    /// This memory is to be preserved by the UEFI OS loader and OS in the
    /// working and ACPI S1–S4 states. This memory may also have other
    /// attributes that are defined by the processor implementation.
    PalCode,

    /// # before ExitBootServices()
    ///
    /// A memory region that operates as EfiConventionalMemory. However, it
    /// happens to also support byte-addressable non-volatility.
    ///
    /// # after ExitBootServices()
    ///
    /// A memory region that operates as EfiConventionalMemory. However, it
    /// happens to also support byte-addressable non-volatility.
    PersistentMemory,

    MaxMemoryType,
}

#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
pub struct PhysicalAddress(pub u64);

/// Frees memory pages.
///
/// # Parameters
///
/// * Memory - The base physical address of the pages to be freed.
/// * Pages - The number of contiguous 4KiB pages to free.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The requested memory pages were freed.
/// * EFI_NOT_FOUND - The requested memory pages were not allocated with
///   AllocatePages().
/// * EFI_INVALID_PARAMETER - Memory is not a page-aligned address or Pages is
///   invalid.
pub type FreePages = extern "C" fn(PhysicalAddress, usize) -> Status;

/// Returns the current memory map.
///
/// # Parameters
///
/// * MemoryMapSize - A pointer to the size, in bytes, of the MemoryMap buffer.
///   On input, this is the size of the buffer allocated by the caller. On
///   output, it is the size of the buffer returned by the firmware if the
///   buffer was large enough, or the size of the buffer needed to contain the
///   map if the buffer was too small.
/// * MemoryMap - A pointer to the buffer in which firmware places the current
///   memory map. The map is an array of EFI_MEMORY_DESCRIPTORs.
/// * MapKey - A pointer to the location in which firmware returns the key for
///   the current memory map.
/// * DescriptorSize - A pointer to the location in which firmware returns the
///   size, in bytes, of an individual EFI_MEMORY_DESCRIPTOR.
/// * DescriptorVersion - A pointer to the location in which firmware returns
///   the versionnumber associated with the EFI_MEMORY_DESCRIPTOR.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The memory map was returned in the MemoryMap buffer.
/// * EFI_BUFFER_TOO_SMALL - The MemoryMap buffer was too small. The current
///   buffer size needed to hold the memory map is returned in MemoryMapSize.
/// * EFI_INVALID_PARAMETER - MemoryMapSize is NULL.
/// * EFI_INVALID_PARAMETER - The MemoryMap buffer is not too small and
///   MemoryMap is NULL.
pub type GetMemoryMap =
    extern "C" fn(&mut usize, *mut MemoryDescriptor, &mut usize, &mut usize, &mut u32) -> Status;

// EFI_GET_MEMORY_MAP Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct MemoryDescriptor {
    /// Type of the memory region.
    pub r#type: u32,

    /// Physical address of the first byte in the memory region. PhysicalStart
    /// must be aligned on a 4KiB boundary, and must not be above
    /// 0xfffffffffffff000.
    pub physical_start: PhysicalAddress,

    /// Virtual address of the first byte in the memory region. VirtualStart
    /// must be aligned on a 4KiB boundary, and must not be above
    /// 0xfffffffffffff000.
    pub virtual_start: VirtualAddress,

    /// Number of 4KiB pages in the memory region. NumberOfPages must not be 0,
    /// and must not be any value that would represent a memory page with a
    /// start address, either physical or virtual, above 0xfffffffffffff000
    pub number_of_pages: u64,

    /// Attributes of the memory region that describe the bit mask of
    /// capabilities for that memory region, and not necessarily the current
    /// settings for that memory region.
    pub attribute: u64,
}

#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
pub struct VirtualAddress(pub u64);

/// Allocates pool memory.
///
/// # Parameters
///
/// * PoolType - The type of pool to allocate. Type EFI_MEMORY_TYPE is defined
///   in the EFI_BOOT_SERVICES.AllocatePages() function description. PoolType
///   values in the range 0x70000000..0x7FFFFFFF are reserved for OEM use.
///   PoolType values in the range 0x80000000..0xFFFFFFFF are reserved for use
///   by UEFI OS loaders that are provided by operating system vendors.
/// * Size - The number of bytes to allocate from the pool.
/// * Buffer - A pointer to a pointer to the allocated buffer if the call
///   succeeds; undefined otherwise.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The requested number of bytes was allocated.
/// * EFI_OUT_OF_RESOURCES - The pool requested could not be allocated.
/// * EFI_INVALID_PARAMETER - PoolType is in the range
///   EfiMaxMemoryType..0x6FFFFFFF.
/// * EFI_INVALID_PARAMETER - PoolType is EfiPersistentMemory.
/// * EFI_INVALID_PARAMETER - Buffer is NULL.
pub type AllocatePool = extern "C" fn(MemoryType, usize, &mut *mut core::ffi::c_void) -> Status;

/// Returns pool memory to the system.
///
/// # Parameters
///
/// * Buffer - Pointer to the buffer to free.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The memory was returned to the system.
/// * EFI_INVALID_PARAMETER - Buffer was invalid.
pub type FreePool = extern "C" fn(*mut core::ffi::c_void) -> Status;

/// Installs a protocol interface on a device handle. If the handle does not
/// exist, it is created and added to the list of handles in the system.
/// InstallMultipleProtocolInterfaces() performs more error checking than
/// InstallProtocolInterface(), so it is recommended that
/// InstallMultipleProtocolInterfaces() be used in place of
/// InstallProtocolInterface()
///
/// # Parameters
///
/// * Handle - A pointer to the EFI_HANDLE on which the interface is to be
///   installed. If *Handle is NULL on input, a new handle is created and
///   returned on output. If *Handle is not NULL on input, the protocol is added
///   to the handle, and the handle is returned unmodified. Thetype EFI_HANDLE
///   is defined in “Related Definitions.” If *Handle is not a valid handle,
///   then EFI_INVALID_PARAMETER is returned.
/// * Protocol - The numeric ID of the protocol interface. It is the caller's
///   responsibility to pass in a valid GUID. See "Wired For Management
///   Baseline" for adescription of valid GUID values.
/// * InterfaceType - Indicates whether Interface is supplied in native form.
///   This value indicates the original execution environment of the request.
/// * Interface - A pointer to the protocol interface. The Interface must adhere
///   to the structure defined by Protocol. NULL can be used if a structure is
///   not associated with Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The protocol interface was installed.
/// * EFI_OUT_OF_RESOURCES - Space for a new handle could not be allocated.
/// * EFI_INVALID_PARAMETER - Handle is NULL
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_INVALID_PARAMETER - InterfaceType is not EFI_NATIVE_INTERFACE.
/// * EFI_INVALID_PARAMETER - Protocol is already installed on the handle
///   specified by Handle.
pub type InstallProtocolInterface =
    extern "C" fn(&mut Handle, &Guid, InterfaceType, *mut core::ffi::c_void) -> Status;

// EFI_INSTALL_PROTOCOL_INTERFACE Related Definitions

/// A collection of related interfaces.
#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
pub struct Handle(pub *mut core::ffi::c_void);

/// 128-bit buffer containing a unique identifier value. Unless otherwise
/// specified, aligned on a 64-bit boundary.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Guid(pub u32, pub u16, pub u16, pub [u8; 8]);

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum InterfaceType {
    NativeInterface,
}

/// Removes a protocol interface from a device handle. It is recommended that
/// UninstallMultipleProtocolInterfaces() be used in place of
/// UninstallProtocolInterface().
///
/// # Parameters
///
/// * Handle - The handle on which the interface was installed. If Handle is not
///   a valid handle, then EFI_INVALID_PARAMETER is returned.
/// * Protocol - The numeric ID of the interface. It is the caller’s
///   responsibility to pass in a valid GUID.
/// * Interface - A pointer to the interface. NULL can be used if a structure is
///   not associated with Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The interface was removed.
/// * EFI_NOT_FOUND - The interface was not found.
/// * EFI_ACCESS_DENIED - The interface was not removed because the interface is
///   still being used by a driver.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
pub type UninstallProtocolInterface =
    extern "C" fn(Handle, &Guid, *mut core::ffi::c_void) -> Status;

/// Reinstalls a protocol interface on a device handle.
///
/// # Parameters
///
/// * Handle - Handle on which the interface is to be reinstalled. If Handle is
///   not a valid handle, then EFI_INVALID_PARAMETER is returned.
/// * Protocol - The numeric ID of the interface. It is the caller’s
///   responsibility to pass in a valid GUID.
/// * OldInterface - A pointer to the old interface. NULL can be used if a
///   structure is not associated with Protocol.
/// * NewInterface - A pointer to the new interface. NULL can be used if a
///   structure is not associated with Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The protocol interface was reinstalled.
/// * EFI_NOT_FOUND - The OldInterface on the handle was not found.
/// * EFI_ACCESS_DENIED - The protocol interface could not be reinstalled,
///   because OldInterface is still being used by a driver that will not release
///   it.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
pub type ReinstallProtocolInterface =
    extern "C" fn(Handle, &Guid, *mut core::ffi::c_void, *mut core::ffi::c_void) -> Status;

/// Creates an event that is to be signaled whenever an interface is installed
/// for a specified protocol.
///
/// # Parameters
///
/// * Protocol - The numeric ID of the protocol for which the event is to be
///   registered.
/// * Event - Event that is to be signaled whenever a protocol interface is
///   registered for Protocol. The same EFI_EVENT may be used for multiple
///   protocol notify registrations.
/// * Registration - A pointer to a memory location to receive the registration
///   value. This value must be saved and used by the notification function of
///   Eventto retrieve the list of handles that have added a protocol interface
///   of type Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The notification event has been registered.
/// * EFI_OUT_OF_RESOURCES - Space for the notification event could not be
///   allocated.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_INVALID_PARAMETER - Event is NULL.
/// * EFI_INVALID_PARAMETER - Registration is NULL.
pub type RegisterProtocolNotify =
    extern "C" fn(&Guid, Event, &mut *mut core::ffi::c_void) -> Status;

/// Returns an array of handles that support a specified protocol.
///
/// # Parameters
///
/// * SearchType - Specifies which handle(s) are to be returned.
/// * Protocol - Specifies the protocol to search by. This parameter is only
///   valid if SearchType is ByProtocol.
/// * SearchKey - Specifies the search key. This parameter is ignored if
///   SearchType is AllHandles or ByProtocol. If SearchType is ByRegisterNotify,
///   the parameter must be the Registration value returned by function
///   EFI_BOOT_SERVICES.RegisterProtocolNotify()
/// * BufferSize - On input, the size in bytes of Buffer. On output, the size in
///   bytes of the array returned in Buffer (if the buffer was large enough) or
///   the size, in bytes, of the buffer needed to obtain the array (if the
///   buffer was not large enough).
/// * Buffer - The buffer in which the array is returned.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The array of handles was returned.
/// * EFI_NOT_FOUND - No handles match the search.
/// * EFI_BUFFER_TOO_SMALL - The BufferSize is too small for the result.
///   BufferSize has been updated with the size needed to complete the request.
/// * EFI_INVALID_PARAMETER - SearchType is not a member of
///   EFI_LOCATE_SEARCH_TYPE.
/// * EFI_INVALID_PARAMETER - SearchType is ByRegisterNotify and SearchKey is
///   NULL.
/// * EFI_INVALID_PARAMETER - SearchType is ByProtocol and Protocol is NULL.
/// * EFI_INVALID_PARAMETER - One or more matches are found and BufferSize is
///   NULL.
/// * EFI_INVALID_PARAMETER - BufferSize is large enough for the result and
///   Buffer is NULL.
pub type LocateHandle = extern "C" fn(
    LocateSearchType,
    &Guid,
    *mut core::ffi::c_void,
    &mut usize,
    &mut Handle,
) -> Status;

// EFI_LOCATE_HANDLE Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum LocateSearchType {
    /// Protocol and SearchKey are ignored and the function returns an array of
    /// every handle in the system.
    AllHandles,

    /// SearchKey supplies the Registration value returned by
    /// EFI_BOOT_SERVICES.RegisterProtocolNotify(). The function returns the
    /// next handle that is new for the registration. Only one handle is
    /// returned at a time, starting with the first, and the caller must loop
    /// until no more handles are returned. Protocol is ignored for this search
    /// type.
    ByRegisterNotify,

    /// All handles that support Protocol are returned. SearchKey is ignored for
    /// this search type.
    ByProtocol,
}

/// Queries a handle to determine if it supports a specified protocol.
///
/// The HandleProtocol() function is still available for use by old EFI
/// applications and drivers. However, all new applications and drivers should
/// use EFI_BOOT_SERVICES.OpenProtocol() in place of HandleProtocol().
///
/// # Parameters
///
/// * Handle - The handle being queried. If Handle is NULL, then
///   EFI_INVALID_PARAMETER is returned.
/// * Protocol - The published unique identifier of the protocol. It is the
///   caller’s responsibility to pass in a valid GUID.
/// * Interface - Supplies the address where a pointer to the corresponding
///   Protocol Interface is returned. NULL will be returned in *Interface if a
///   structure is not associated with Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The interface information for the specified protocol was
///   returned.
/// * EFI_UNSUPPORTED - The device does not support the specified protocol.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_INVALID_PARAMETER - Interface is NULL.
pub type HandleProtocol = extern "C" fn(Handle, &Guid, &mut *mut core::ffi::c_void) -> Status;

/// Locates the handle to a device on the device path that supports the
/// specified protocol.
///
/// # Parameters
///
/// * Protocol - The protocol to search for.
/// * DevicePath - On input, a pointer to a pointer to the device path. On
///   output, the device path pointer is modified to point to the remaining part
///   of the device path—that is, when the function finds the closest handle, it
///   splits the device path into two parts, stripping off the front part, and
///   returning the remaining portion.
/// * Device - A pointer to the returned device handle.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The resulting handle was returned.
/// * EFI_NOT_FOUND - No handles matched the search.
/// * EFI_INVALID_PARAMETER - Protocol is NULL
/// * EFI_INVALID_PARAMETER - DevicePath is NULL.
/// * EFI_INVALID_PARAMETER - A handle matched the search and Device is NULL.
pub type LocateDevicePath = extern "C" fn(&Guid, &&mut DevicePathProtocol, &mut Handle) -> Status;

/// Queries a handle to determine if it supports a specified protocol. If the
/// protocol is supported by the handle, it opens the protocol on behalf of the
/// calling agent. This is an extended version of the EFI boot service
/// EFI_BOOT_SERVICES.HandleProtocol().
///
/// # Parameters
///
/// Handle - The handle for the protocol interface that is being opened.
/// Protocol - The published unique identifier of the protocol. It is the
/// caller’s responsibility to pass in a valid GUID. Interface - Supplies the
/// address where a pointer to the corresponding Protocol Interface is returned.
/// NULL will be returned in *Interface if a structure is not associated with
/// Protocol. This parameter is optional, and will be ignored if Attributes is
/// EFI_OPEN_PROTOCOL_TEST_PROTOCOL. AgentHandle - The handle of the agent that
/// is opening the protocol interface specified by Protocol and Interface. For
/// agents that follow the UEFIDriver Model, this parameter is the handle that
/// contains the EFI_DRIVER_BINDING_PROTOCOL instance that is produced by the
/// UEFI driver that is opening the protocol interface. For UEFI applications,
/// this is the image handle of the UEFI application that is opening the
/// protocol interface. For applications that use HandleProtocol() to open a
/// protocol interface, this parameter is the image handle of the EFI firmware.
/// ControllerHandle - f the agent that is opening a protocol is a driver that
/// follows the UEFI Driver Model, then this parameter is the controller handle
/// that requires the protocol interface. If the agent does not follow the
/// UEFIDriver Model, then this parameter is optional and may be NULL.
/// Attributes - The open mode of the protocol interface specified by Handle and
/// Protocol.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - An item was added to the open list for the protocol
///   interface, and the protocol interface was returned in Interface.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_INVALID_PARAMETER - Interface is NULL, and Attributes is not
///   TEST_PROTOCOL.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_UNSUPPORTED - Handle does not support Protocol.
/// * EFI_INVALID_PARAMETER - Attributes is not a legal value.
/// * EFI_INVALID_PARAMETER - Attributes is BY_CHILD_CONTROLLER and AgentHandle
///   is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_DRIVER and AgentHandle is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_DRIVER|EXCLUSIVE and AgentHandle
///   is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is EXCLUSIVE and AgentHandle is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_CHILD_CONTROLLER and
///   ControllerHandle is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_DRIVER and ControllerHandle is
///   NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_DRIVER|EXCLUSIVE and
///   ControllerHandle is NULL.
/// * EFI_INVALID_PARAMETER - Attributes is BY_CHILD_CONTROLLER and Handle is
///   identical to ControllerHandle.
/// * EFI_ACCESS_DENIED - Attributes is BY_DRIVER and there is an item on the
///   open list with an attribute of BY_DRIVER|EXCLUSIVE or EXCLUSIVE.
/// * EFI_ACCESS_DENIED - Attributes is BY_DRIVER|EXCLUSIVE and there is an item
///   on the open list with an attribute of EXCLUSIVE.
/// * EFI_ACCESS_DENIED - Attributes is EXCLUSIVE and there is an item on the
///   open list with an attribute of BY_DRIVER|EXCLUSIVE or EXCLUSIVE.
/// * EFI_ALREADY_STARTED - Attributes is BY_DRIVER and there is an item on the
///   open list with an attribute of BY_DRIVER whose agent handle is the same as
///   AgentHandle.
/// * EFI_ACCESS_DENIED - Attributes is BY_DRIVER and there is an item on the
///   open list with an attribute of BY_DRIVER whose agent handle is different
///   than AgentHandle.
/// * EFI_ALREADY_STARTED - Attributes is BY_DRIVER|EXCLUSIVE and there is an
///   item on the open list with an attribute of BY_DRIVER|EXCLUSIVE whose agent
///   handle is the same as AgentHandle.
/// * EFI_ACCESS_DENIED - Attributes is BY_DRIVER|EXCLUSIVE and there is an item
///   on the open list with an attribute of BY_DRIVER|EXCLUSIVE whose agent
///   handle is different than AgentHandle.
/// * EFI_ACCESS_DENIED - Attributes is BY_DRIVER|EXCLSUIVE or EXCLUSIVE and
///   there are items in the open list with an attribute of BY_DRIVER that could
///   not be removed when EFI_BOOT_SERVICES.DisconnectController() was called
///   for that open item.
pub type OpenProtocol =
    extern "C" fn(Handle, &Guid, &mut *mut core::ffi::c_void, Handle, Handle, u32) -> Status;

/// Closes a protocol on a handle that was opened using
/// EFI_BOOT_SERVICES.OpenProtocol().
///
/// # Parameters
///
/// * Handle - The handle for the protocol interface that was previously opened
///   with OpenProtocol(), and is now being closed.
/// * Protocol - The published unique identifier of the protocol. It is the
///   caller’s responsibility to pass in a valid GUID.
/// * AgentHandle - The handle of the agent that is closing the protocol
///   interface. For agents that follow the UEFI Driver Model, this parameter is
///   the handle that contains the EFI_DRIVER_BINDING_PROTOCOLinstance that is
///   produced by the UEFI driver that is opening the protocol interface. For
///   UEFI applications, this is the image handle of the UEFI application. For
///   applications that used EFI_BOOT_SERVICES.HandleProtocol() to open the
///   protocol interface, this will be the image handle of the EFI firmware.
/// * ControllerHandle - If the agent that opened a protocol is a driver that
///   follows the UEFIDriver Model, then this parameter is the controller handle
///   that required the protocol interface. If the agent does not follow the
///   UEFIDriver Model, then this parameter is optional and may be NULL.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The protocol instance was closed.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - AgentHandle is NULL.
/// * EFI_INVALID_PARAMETER - ControllerHandle is not NULL and ControllerHandle
///   is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_NOT_FOUND - Handle does not support the protocol specified by
///   Protocol.
/// * EFI_NOT_FOUND - The protocol interface specified by Handle and Protocol is
///   not currently open by AgentHandle and ControllerHandle.
pub type CloseProtocol = extern "C" fn(Handle, &Guid, Handle, Handle) -> Status;

/// Retrieves the list of agents that currently have a protocol interface
/// opened.
///
/// # Parameters
///
/// * Handle - The handle for the protocol interface that is being queried.
/// * Protocol - The published unique identifier of the protocol. It is the
///   caller’s responsibility to pass in a valid GUID.
/// * EntryBuffer - A pointer to a buffer of open protocol information in the
///   form of EFI_OPEN_PROTOCOL_INFORMATION_ENTRY structures. The buffer is
///   allocated by this service, and it is the caller's responsibility to free
///   this buffer when the caller no longer requires the buffer's contents.
/// * EntryCount - A pointer to the number of entries in EntryBuffer.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The open protocol information was returned in EntryBuffer,
///   and the number of entries was returned EntryCount.
/// * EFI_NOT_FOUND - Handle does not support the protocol specified by
///   Protocol.
/// * EFI_OUT_OF_RESOURCES - There are not enough resources available to
///   allocate EntryBuffer.
pub type OpenProtocolInformation =
    extern "C" fn(Handle, &Guid, &&mut OpenProtocolInformationEntry, &mut usize) -> Status;

// EFI_OPEN_PROTOCOL_INFORMATION Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct OpenProtocolInformationEntry {
    pub agent_handle: Handle,
    pub controller_handle: Handle,
    pub attributes: u32,
    pub open_count: u32,
}

/// Connects one or more drivers to a controller.
///
/// # Parameters
///
/// * ControllerHandle - The handle of the controller to which driver(s) are to
///   be connected.
/// * DriverImageHandle - A pointer to an ordered list handles that support the
///   EFI_DRIVER_BINDING_PROTOCOL. The list is terminated by a NULL handle
///   value. These handles are candidates for the Driver Binding Protocol(s)
///   that will manage the controller specified by ControllerHandle. This is an
///   optional parameter that may be NULL. This parameter is typically used to
///   debug new drivers.
/// * RemainingDevicePath - A pointer to the device path that specifies a child
///   of the controller specified by ControllerHandle. This is an optional
///   parameter that may be NULL. If it is NULL, then handles for all the
///   children of ControllerHandle will be created. This parameter is passed
///   unchanged to the Supported() and Start() services of the
///   EFI_DRIVER_BINDING_PROTOCOL attached to ControllerHandle.
/// * Recursive - If TRUE, then ConnectController() is called recursively until
///   the entire tree of controllers below the controller specified by
///   ControllerHandle have been created. If FALSE, then the tree of controllers
///   is only expanded one level.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - One or more drivers were connected to ControllerHandle.
/// * EFI_SUCCESS - No drivers were connected to ControllerHandle, but
///   RemainingDevicePath is not NULL, and it is an End Device Path Node.
/// * EFI_INVALID_PARAMETER - ControllerHandle is NULL.
/// * EFI_NOT_FOUND - There are no EFI_DRIVER_BINDING_PROTOCOL instances present
///   in the system.
/// * EFI_NOT_FOUND - No drivers were connected to ControllerHandle.
/// * EFI_SECURITY_VIOLATION - The user has no permission to start UEFI device
///   drivers on the device path associated with the ControllerHandle or
///   specified by the RemainingDevicePath.
pub type ConnectController = extern "C" fn(Handle, &Handle, &DevicePathProtocol, bool) -> Status;

/// Disconnects one or more drivers from a controller.
///
/// # Parameters
///
/// * ControllerHandle - The handle of the controller from which driver(s) are
///   to be disconnected.
/// * DriverImageHandle - The driver to disconnect from ControllerHandle. If
///   DriverImageHandle is NULL, then all the drivers currently managing
///   ControllerHandle are disconnected from ControllerHandle.
/// * ChildHandle - The handle of the child to destroy. If ChildHandle is NULL,
///   then all the children of ControllerHandle are destroyed before the drivers
///   are disconnected from ControllerHandle.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - One or more drivers were disconnected from the controller.
/// * EFI_SUCCESS - On entry, no drivers are managing ControllerHandle.
/// * EFI_SUCCESS - DriverImageHandle is not NULL, and on entry
///   DriverImageHandle is not managing ControllerHandle.
/// * EFI_INVALID_PARAMETER - ControllerHandle is NULL.
/// * EFI_INVALID_PARAMETER - DriverImageHandle is not NULL, and it is not a
///   valid EFI_HANDLE.
/// * EFI_INVALID_PARAMETER - ChildHandle is not NULL, and it is not a valid
///   EFI_HANDLE.
/// * EFI_OUT_OF_RESOURCES - There are not enough resources available to
///   disconnect any drivers from ControllerHandle.
/// * EFI_DEVICE_ERROR - The controller could not be disconnected because of a
///   device error.
/// * EFI_INVALID_PARAMETER - DriverImageHandle does not support the
///   EFI_DRIVER_BINDING_PROTOCOL.
pub type DisconnectController = extern "C" fn(Handle, Handle, Handle) -> Status;

/// Retrieves the list of protocol interface GUIDs that are installed on a
/// handle in a buffer allocated from pool.
///
/// # Parameters
///
/// * Handle - The handle from which to retrieve the list of protocol interface
///   GUIDs.
/// * ProtocolBuffer - A pointer to the list of protocol interface GUID pointers
///   that are installed on Handle. This buffer is allocated with a call to the
///   Boot Service EFI_BOOT_SERVICES.AllocatePool(). It is the caller's
///   responsibility to call the Boot Service EFI_BOOT_SERVICES.FreePool() when
///   the caller no longer requires the contents of ProtocolBuffer.
/// * ProtocolBufferCount - A pointer to the number of GUID pointers present in
///   ProtocolBuffer.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The list of protocol interface GUIDs installed on Handle was
///   returned in ProtocolBuffer. The number of protocol interface GUIDs was
///   returned in ProtocolBufferCount.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - ProtocolBuffer is NULL.
/// * EFI_INVALID_PARAMETER - ProtocolBufferCount is NULL.
/// * EFI_OUT_OF_RESOURCES - There is not enough pool memory to store the
///   results.
pub type ProtocolsPerHandle = extern "C" fn(Handle, &&&mut Guid, &mut usize) -> Status;

/// Returns an array of handles that support the requested protocol in a buffer
/// allocated from pool.
///
/// # Parameters
///
/// * SearchType - Specifies which handle(s) are to be returned.
/// * Protocol - Provides the protocol to search by. This parameter is only
///   valid for a SearchType of ByProtocol.
/// * SearchKey - Supplies the search key depending on the SearchType.
/// * NoHandles - The number of handles returned in Buffer.
/// * Buffer - A pointer to the buffer to return the requested array of handles
///   that support Protocol. This buffer is allocated with a call to the Boot
///   Service EFI_BOOT_SERVICES.AllocatePool(). It is the caller's
///   responsibility to call the Boot Service EFI_BOOT_SERVICES.FreePool() when
///   the caller no longer requires the contents of Buffer.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The array of handles was returned in Buffer, and the number
///   of handles in Buffer was returned in NoHandles.
/// * EFI_INVALID_PARAMETER - NoHandles is NULL
/// * EFI_INVALID_PARAMETER - Buffer is NULL
/// * EFI_NOT_FOUND - No handles match the search.
/// * EFI_OUT_OF_RESOURCES - There is not enough pool memory to store the
///   matching results.
pub type LocateHandleBuffer =
    extern "C" fn(LocateSearchType, &Guid, *mut core::ffi::c_void, &usize, &&mut Handle) -> Status;

/// Returns the first protocol instance that matches the given protocol.
///
/// # Parameters
///
/// * Protocol - Provides the protocol to search for.
/// * Registration - Optional registration key returned from
///   EFI_BOOT_SERVICES.RegisterProtocolNotify(). If Registration is NULL, then
///   it is ignored.
/// * Interface - On return, a pointer to the first interface that matches
///   Protocol and Registration.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - A protocol instance matching Protocol was found and returned
///   in Interface.
/// * EFI_INVALID_PARAMETER - Interface is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is NULL.
/// * EFI_NOT_FOUND - No protocol instances were found that match Protocol
///   andRegistration.
pub type LocateProtocol = fn(&Guid, *mut core::ffi::c_void, &mut *mut core::ffi::c_void) -> Status;

/// Installs one or more protocol interfaces into the boot services environment.
///
/// # Parameters
///
/// * Handle - The pointer to a handle to install the new protocol interfaces
///   on, or a pointer to NULL if a new handle is to be allocated.
/// * `...` - A variable argument list containing pairs of protocol GUIDs and
///   protocol interfaces.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - All the protocol interfaces were installed.
/// * EFI_ALREADY_STARTED - A Device Path Protocol instance was passed in that
///   is already present in the handle database.
/// * EFI_OUT_OF_RESOURCES - There was not enough memory in pool to install all
///   the protocols.
/// * EFI_INVALID_PARAMETER - Handle is NULL.
/// * EFI_INVALID_PARAMETER - Protocol is already installed on the handle
///   specified by Handle.
pub type InstallMultipleProtocolInterfaces = extern "C" fn(&Handle, ...) -> Status;

/// Removes one or more protocol interfaces into the boot services environment.
///
/// # Parameters
///
/// * Handle - The handle to remove the protocol interfaces from.
/// * `...` - A variable argument list containing pairs of protocol GUIDs and
///   protocol interfaces.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - All the protocol interfaces were removed.
/// * EFI_INVALID_PARAMETER - One of the protocol interfaces was not previously
///   installed on Handle.
pub type UninstallMultipleProtocolInterfaces = extern "C" fn(Handle, ...) -> Status;

/// Loads an EFI image into memory.
///
/// # Parameters
///
/// * BootPolicy - If TRUE, indicates that the request originates from the boot
///   manager, and that the boot manager is attempting to load DevicePath as a
///   boot selection. Ignored if SourceBuffer is not NULL.
/// * ParentImageHandle - The caller’s image handle. Type EFI_HANDLE is defined
///   in the EFI_BOOT_SERVICES.InstallProtocolInterface() function description.
///   This field is used to initialize the ParentHandle field of the
///   EFI_LOADED_IMAGE_PROTOCOL for the image that is being loaded.
/// * DevicePath - The DeviceHandle specific file path from which the image is
///   loaded.
/// * SourceBuffer - If not NULL, a pointer to the memory location containing a
///   copy of the image to be loaded.
/// * SourceSize - The size in bytes of SourceBuffer. Ignored if SourceBuffer is
///   NULL.
/// * ImageHandle - Pointer to the returned image handle that is created when
///   the image is successfully loaded.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - Image was loaded into memory correctly.
/// * EFI_NOT_FOUND - Both SourceBuffer and DevicePath are NULL.
/// * EFI_INVALID_PARAMETER - One of the parameters has an invalid value.
/// * EFI_INVALID_PARAMETER - ImageHandle is NULL.
/// * EFI_INVALID_PARAMETER - ParentImageHandle is NULL.
/// * EFI_UNSUPPORTED - The image type is not supported.
/// * EFI_OUT_OF_RESOURCES - Image was not loaded due to insufficient resources.
/// * EFI_LOAD_ERROR - Image was not loaded because the image format was corrupt
///   or not understood.
/// * EFI_DEVICE_ERROR - Image was not loaded because the device returned a read
///   error.
/// * EFI_ACCESS_DENIED - Image was not loaded because the platform policy
///   prohibits the image from being loaded. NULL is returned in *ImageHandle.
/// * EFI_SECURITY_VIOLATION - Image was loaded and an ImageHandle was created
///   with a valid EFI_LOADED_IMAGE_PROTOCOL.. However, the current platform
///   policy specifies that the image should not be started.
pub type ImageLoad = extern "C" fn(
    bool,
    Handle,
    &DevicePathProtocol,
    *mut core::ffi::c_void,
    usize,
    &mut Handle,
) -> Status;

// EFI_IMAGE_LOAD Related Definitions

/// Transfers control to a loaded image's entry point.
///
/// # Parameters
///
/// * ImageHandle - Handle of image to be started.
/// * ExitDataSize - Pointer to the size, in bytes, of ExitData. If ExitData is
///   NULL, then this parameter is ignored and the contents of ExitDataSize are
///   not modified.
/// * ExitData - Pointer to a pointer to a data buffer that includes a
///   Null-terminated string, optionally followed by additional binary data. The
///   string is a description that the caller may use to further indicate the
///   reason for the image's exit.
///
/// # Status Codes Returned
///
/// * EFI_INVALID_PARAMETER - ImageHandle is either an invalid image handle or
///   the image has already been initialized with StartImage
/// * Exit code from image - Exit code from image.
/// * EFI_SECURITY_VIOLATION - The current platform policy specifies that the
///   image should not be started.
pub type ImageStart = extern "C" fn(Handle, &mut usize, &mut &u16) -> Status;

/// Unloads an image.
///
/// # Parameters
///
/// * ImageHandle - Handle that identifies the image to be unloaded.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The image has been unloaded.
/// * EFI_UNSUPPORTED - The image has been started, and does not support unload.
/// * EFI_INVALID_PARAMETER - ImageHandle is not a valid image handle.
/// * Exit code from Unload handler - Exit code from the image's unload
///   function.
pub type ImageUnload = extern "C" fn(Handle) -> Status;

/// Terminates a loaded EFI image and returns control to boot services.
///
/// # Parameters
///
/// * ImageHandle - Handle that identifies the image. This parameter is passed
///   to the image on entry.
/// * ExitStatus - The image's exit code.
/// * ExitDataSize - The size, in bytes, of ExitData. Ignored if ExitStatus is
///   EFI_SUCCESS.
/// * ExitData - Pointer to a data buffer that includes a Null-terminated
///   string, optionally followed by additional binary data. The string is a
///   description that the caller may use to further indicate the reason for the
///   image’s exit. ExitData is only valid if ExitStatus is something other than
///   EFI_SUCCESS. The ExitData buffer must be allocated by calling
///   EFI_BOOT_SERVICES.AllocatePool().
///
/// # Status Codes Returned
///
/// * (Does not return.) - Image exit. Control is returned to the StartImage()
///   call that invoked the image specified by ImageHandle.
/// * EFI_SUCCESS - The image specified by ImageHandle was unloaded. This
///   condition only occurs for images that have been loaded with LoadImage()
///   but have not been started with StartImage().
/// * EFI_INVALID_PARAMETER - The image specified by ImageHandle has been loaded
///   and started with LoadImage() and StartImage(), but the image is not the
///   currently executing image.
pub type Exit = extern "C" fn(Handle, Status, usize, *const u16) -> Status;

/// Terminates all boot services.
///
/// # Parameters
///
/// * ImageHandle - Handle that identifies the exiting image.
/// * MapKey - Key to the latest memory map.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - Boot services have been terminated.
/// * EFI_INVALID_PARAMETER - MapKey is incorrect.
pub type ExitBootServices = extern "C" fn(Handle, usize) -> Status;

/// Sets the system’s watchdog timer.
///
/// # Parameters
///
/// * `Timeout` - The number of seconds to set the watchdog timer to. A value of
///   zero disables the timer.
/// * `WatchdogCode` - The numeric code to log on a watchdog timer timeout
///   event. The firmware reserves codes 0x0000 to 0xFFFF. Loaders and
///   operatingsystems may use other timeout codes.
/// * `DataSize` - The size, in bytes, of WatchdogData.
/// * `WatchdogData` - A data buffer that includes a Null-terminated string,
///   optionally followed by additional binary data. The string is a description
///   that the call may use to further indicate the reason to be logged with a
///   watchdog event.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The timeout has been set.
/// * `EFI_INVALID_PARAMETER` - The supplied WatchdogCode is invalid.
/// * `EFI_UNSUPPORTED` - The system does not have a watchdog timer.
/// * `EFI_DEVICE_ERROR` - The watch dog timer could not be programmed due to a
///   hardware error.
pub type SetWatchdogTimer = extern "C" fn(usize, u64, usize, u16) -> Status;

/// Induces a fine-grained stall.
///
/// # Parameters
///
/// * `Microseconds` - The number of microseconds to stall execution.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - Execution was stalled at least the requested number
///   ofMicroseconds.
pub type Stall = extern "C" fn(usize) -> Status;

/// The CopyMem() function copies the contents of one buffer to another buffer.
///
/// # Parameters
///
/// * `Destination` - Pointer to the destination buffer of the memory copy.
/// * `Source` - Pointer to the source buffer of the memory copy.
/// * `Length` - Number of bytes to copy from Source to Destination.
pub type CopyMem = extern "C" fn(*mut core::ffi::c_void, *mut core::ffi::c_void, usize);

/// The SetMem() function fills a buffer with a specified value.
///
/// # Parameters
///
/// * `Buffer` - Pointer to the buffer to fill.
/// * `Size` - Number of bytes in Buffer to fill.
/// * `Value` - Value to fill Buffer with.
pub type SetMem = extern "C" fn(*mut core::ffi::c_void, usize, u8);

/// Returns a monotonically increasing count for the platform.
///
/// # Parameters
///
/// * `Count` - Pointer to returned value.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The next monotonic count was returned.
/// * `EFI_DEVICE_ERROR` - The device is not functioning properly.
/// * `EFI_INVALID_PARAMETER` - Count is NULL.
pub type GetNextMonotonicCount = extern "C" fn(&mut u64) -> Status;

/// Adds, updates, or removes a configuration table entry from the EFI System
/// Table.
///
/// # Parameters
///
/// * `Guid` - A pointer to the GUID for the entry to add, update, or remove.
/// * `Table` - A pointer to the configuration table for the entry to add,
///   update, or remove. May be NULL.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The (Guid, Table) pair was added, updated, or removed.
/// * `EFI_INVALID_PARAMETER` - Guid is NULL.
/// * `EFI_NOT_FOUND` - An attempt was made to delete a nonexistent entry.
/// * `EFI_OUT_OF_RESOURCES` - There is not enough memory available to complete
///   the operation.
pub type InstallConfigurationTable = extern "C" fn(&Guid, *mut core::ffi::c_void) -> Status;

/// Computes and returns a 32-bit CRC for a data buffer.
///
/// # Parameters
///
/// * `Data` - A pointer to the buffer on which the 32-bit CRC is to be
///   computed.
/// * `DataSize` - The number of bytes in the buffer Data.
/// * `Crc32` - The 32-bit CRC that was computed for the data buffer specified
///   by Data and DataSize.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The 32-bit CRC was computed for the data buffer and
///   returned in Crc32.
/// * `EFI_INVALID_PARAMETER` - Data is NULL.
/// * `EFI_INVALID_PARAMETER` - Crc32 is NULL.
/// * `EFI_INVALID_PARAMETER` - DataSize is 0.
pub type CalculateCrc32 = extern "C" fn(*mut core::ffi::c_void, usize, &mut u32) -> Status;

/// # Services - Runtime Services

/// Returns the value of a variable.
///
/// # Parameters
///
/// * VariableName - A Null-terminated string that is the name of the vendor’s
///   variable.
/// * VendorGuid - A unique identifier for the vendor.
/// * Attributes - If not NULL, a pointer to the memory location to return the
///   attributes bitmask for the variable. If not NULL, then Attributes is set
///   on output both when EFI_SUCCESS and when EFI_BUFFER_TOO_SMALL is returned.
/// * DataSize - On input, the size in bytes of the return Data buffer. On
///   output the size of data returned in Data.
/// * Data - The buffer to return the contents of the variable. May be NULL with
///   a zero DataSize in order to determine the size buffer needed.
///
/// # Status Codes Returend
///
/// * EFI_SUCCESS - The function completed successfully.
/// * EFI_NOT_FOUND - The variable was not found.
/// * EFI_BUFFER_TOO_SMALL - The DataSize is too small for the result. DataSize
///   has been updated with the size needed to complete the request. If
///   Attributes is not NULL, then the attributes bitmask for the variable has
///   been stored to the memory location pointed-to by Attributes.
/// * EFI_INVALID_PARAMETER - VariableName is NULL.
/// * EFI_INVALID_PARAMETER - VendorGuid is NULL.
/// * EFI_INVALID_PARAMETER - DataSize is NULL.
/// * EFI_INVALID_PARAMETER - The DataSize is not too small and Data is NULL.
/// * EFI_DEVICE_ERROR - The variable could not be retrieved due to a hardware
///   error.
/// * EFI_SECURITY_VIOLATION - The variable could not be retrieved due to an
///   authentication failure.
/// * EFI_UNSUPPORTED - After ExitBootServices() has been called, this return
///   code may be returned if no variable storage is supported. The platform
///   mustcorrectly reflect this behavior in the RuntimeServicesSupported
///   variable.
pub type GetVariable =
    extern "C" fn(&u16, &Guid, &mut u32, &usize, *mut core::ffi::c_void) -> Status;

/// Enumerates the current variable names.
///
/// # Parameters
///
/// * VariableNameSize - The size of the VariableName buffer. The size must be
///   large enough to fit input string supplied in VariableName buffer.
/// * VariableName - On input, supplies the last VariableName that was returned
///   by GetNextVariableName(). On output, returns the Null-terminated string of
///   the current variable.
/// * VendorGuid - On input, supplies the last VendorGuid that was returned by
///   GetNextVariableName(). On output, returns the VendorGuidof the current
///   variable.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The function completed successfully.
/// * EFI_NOT_FOUND - The next variable was not found.
/// * EFI_BUFFER_TOO_SMALL - The VariableNameSize is too small for the result.
///   VariableNameSize has been updated with the size needed to complete the
///   request.
/// * EFI_INVALID_PARAMETER - VariableNameSize is NULL.
/// * EFI_INVALID_PARAMETER - VariableName is NULL.
/// * EFI_INVALID_PARAMETER - VendorGuid is NULL.
/// * EFI_INVALID_PARAMETER - The input values of VariableName and VendorGuid
///   are not a name and GUID of an existing variable.
/// * EFI_INVALID_PARAMETER - Null-terminator is not found in the first
///   VariableNameSize bytes of the input VariableName buffer.
/// * EFI_DEVICE_ERROR - The variable name could not be retrieved due to a
///   hardware error.
/// * EFI_UNSUPPORTED - After ExitBootServices() has been called, this return
///   code may be returned if no variable storage is supported. The platform
///   mustcorrectly reflect this behavior in the RuntimeServicesSupported
///   variable.
pub type GetNextVariableName = extern "C" fn(&mut usize, &mut u16, &mut Guid) -> Status;

/// Sets the value of a variable.
///
/// # Parameters
///
/// * VariableName - A Null-terminated string that is the name of the vendor’s
///   variable. Each VariableName is unique for each VendorGuid. VariableName
///   must contain 1 or more characters. If VariableName is an empty string,
///   then EFI_INVALID_PARAMETER is returned.
/// * VendorGuid - A unique identifier for the vendor.
/// * Attributes - Attributes bitmask to set for the variable. Refer to the
///   GetVariable() function description.
/// * DataSize - The size in bytes of the Data buffer. Unless
///   theEFI_VARIABLE_APPEND_WRITE,EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS,
///   EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS, or
///   EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESSattribute is set, a size
///   of zero causes the variable to be deleted. When the
///   EFI_VARIABLE_APPEND_WRITEattribute is set, then a SetVariable() call with
///   a DataSize of zero will not cause any change to the variable value (the
///   timestamp associated with the variable may be updated however, even if no
///   new data value is provided;see the description of the
///   EFI_VARIABLE_AUTHENTICATION_2 descriptor below). In this case the DataSize
///   will not be zero since the EFI_VARIABLE_AUTHENTICATION_2 descriptor will
///   be populated).
/// * Data - The contents for the variable.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The firmware has successfully stored the variable and its
///   data as defined by the Attributes.
/// * EFI_INVALID_PARAMETER - An invalid combination of attribute bits, name,
///   and GUID was supplied, or the DataSize exceeds the maximum allowed.
/// * EFI_INVALID_PARAMETER - VariableName is an empty string.
/// * EFI_OUT_OF_RESOURCES - Not enough storage is available to hold the
///   variable and its data.
/// * EFI_DEVICE_ERROR - The variable could not be saved due to a hardware
///   failure.
/// * EFI_WRITE_PROTECTED - The variable in question is read-only.
/// * EFI_WRITE_PROTECTED - The variable in question cannot be deleted.
/// * EFI_SECURITY_VIOLATION - The variable could not be written due to
///   EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS or
///   EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACESS being set, but the
///   payload does NOT pass the validation check carried out by the firmware.
/// * EFI_NOT_FOUND - The variable trying to be updated or deleted was not
///   found.
/// * EFI_UNSUPPORTED - This call is not supported by this platform at the time
///   the call is made.  The platform must correctly reflect this behavior in
///   the RuntimeServicesSupported variable.
pub type SetVariable = extern "C" fn(&u16, &Guid, u32, usize, *mut core::ffi::c_void) -> Status;

/// Returns information about the EFI variables.
///
/// # Parameters
///
/// * Attributes - Attributes bitmask to specify the type of variables on which
///   to return information. Refer to the GetVariable() function description.The
///   EFI_VARIABLE_APPEND_WRITE attribute, if set in the attributes bitmask,
///   will be ignored.
/// * MaximumVariableStorageSize - On output the maximum size of the storage
///   space available for the EFI variables associated with the attributes
///   specified.
/// * RemainingVariableStorageSize - Returns the remaining size of the storage
///   space available for EFI variables associated with the attributes
///   specified.
/// * MaximumVariableSize - Returns the maximum size of an individual EFI
///   variable associated with the attributes specified.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - Valid answer returned.
/// * EFI_INVALID_PARAMETER - An invalid combination of attribute bits was
///   supplied.
/// * EFI_UNSUPPORTED - The attribute is not supported on this platform, and the
///   MaximumVariableStorageSize, RemainingVariableStorageSize,
///   MaximumVariableSize are undefined.
pub type QueryVariableInfo = extern "C" fn(usize, &mut u64, &mut u64, &mut u64) -> Status;

/// Returns the current time and date information, and the time-keeping
/// capabilities of the hardware platform.
///
/// # Parameters
///
/// * `Time` - A pointer to storage to receive a snapshot of the current time.
/// * `Capabilities` - An optional pointer to a buffer to receive the real time
///   clock device’s capabilities.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The operation completed successfully.
/// * `EFI_INVALID_PARAMETER` - Time is NULL.
/// * `EFI_DEVICE_ERROR` - The time could not be retrieved due to a hardware
///   error.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type GetTime = extern "C" fn(&mut Time, &mut TimeCapabilities) -> Status;

// EFI_GET_TIME Related Definitions

/// This represents the current time information
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Time {
    /// The current local date.
    /// 1900 - 9999
    pub year: u16,

    /// The current local date.
    /// 1 - 12
    pub month: u8,

    /// The current local date.
    /// 1 - 31
    pub day: u8,

    /// The current local time. Nanoseconds report the current fraction of a
    /// second in the device. The format of the time is hh:mm:ss.nnnnnnnnn. A
    /// battery backed real time clock device maintains the date and time. 0
    /// - 23
    pub hour: u8,

    /// The current local time. Nanoseconds report the current fraction of a
    /// second in the device. The format of the time is hh:mm:ss.nnnnnnnnn. A
    /// battery backed real time clock device maintains the date and time. 0
    /// - 59
    pub minute: u8,

    /// The current local time. Nanoseconds report the current fraction of a
    /// second in the device. The format of the time is hh:mm:ss.nnnnnnnnn. A
    /// battery backed real time clock device maintains the date and time. 0
    /// - 59
    pub second: u8,

    pad1: u8,

    /// The current local time. Nanoseconds report the current fraction of a
    /// second in the device. The format of the time is hh:mm:ss.nnnnnnnnn. A
    /// battery backed real time clock device maintains the date and time. 0
    /// - 999,999,999
    pub nanosecond: u32,

    /// The time's offset in minutes from UTC. If the value is
    /// EFI_UNSPECIFIED_TIMEZONE, then the time is interpreted as a local time.
    /// The TimeZone is the number of minutes that the local time is relative to
    /// UTC. To calculate the TimeZone value, follow this equation: Localtime =
    /// UTC - TimeZone. -1440 to 1440 or 2047
    pub time_zone: u16,

    /// A bitmask containing the daylight savings time information for the time.
    /// The EFI_TIME_ADJUST_DAYLIGHT bit indicates if the time is affected by
    /// daylight savings time or not. This value does not indicate that the time
    /// has been adjusted for daylight savings time. It indicates only that it
    /// should be adjusted when the EFI_TIME enters daylight savings time.
    /// If EFI_TIME_IN_DAYLIGHT is set, the time has been adjusted for daylight
    /// savings time. All other bits must be zero.
    ///
    /// When entering daylight saving time, if the time is affected, but hasn't
    /// been adjusted (DST = 1), use the new calculation: 1. The date/time
    /// should be increased by the appropriate amount. 2. The TimeZone
    /// should be decreased by the appropriate amount (EX: +480 changes to +420
    /// when moving from PST to PDT). 3. The Daylight value changes to 3.
    ///
    /// When exiting daylight saving time, if the time is affected and has been
    /// adjusted (DST = 3), use the new calculation: 1. The date/time should
    /// be decreased by the appropriate amount. 2. The TimeZone should be
    /// increased by the appropriate amount. 3. The Daylight value changes
    /// to 1.
    pub daylight: u8,

    pad2: u8,
}

/// This provides the capabilities of the real time clock device as exposed
/// through the EFI interfaces.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TimeCapabilities {
    /// Provides the reporting resolution of the real-time clock device in
    /// counts per second. For a normal PC-AT CMOS RTC device, this value would
    /// be 1 Hz, or 1, to indicate that the device only reports the time to the
    /// resolution of 1 second.
    pub resolution: u32,

    /// Provides the timekeeping accuracy of the real-time clock in an error
    /// rate of 1E-6 parts per million. For a clock with an accuracy of 50 parts
    /// per million, the value in this field would be 50,000,000.
    pub accuracy: u32,

    /// A TRUE indicates that a time set operation clears the device’s time
    /// below the Resolution reporting level. A FALSE indicates that the state
    /// below the Resolution level of the device is not cleared when the time is
    /// set. Normal PC-AT CMOS RTC devices set this value to FALSE.
    pub sets_to_zero: bool,
}

/// Sets the current local time and date information.
///
/// # Parameters
///
/// * `Time` - A pointer to the current time. Type EFI_TIME is defined in the
///   GetTime() function description. Full error checking is performed on the
///   different fields of the EFI_TIME structure (refer to the EFI_TIME
///   definition in the GetTime() function description for full details), and
///   EFI_INVALID_PARAMETER is returned if any field is out of range.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The operation completed successfully.
/// * `EFI_INVALID_PARAMETER` - A time field is out of range.
/// * `EFI_DEVICE_ERROR` - The time could not be set due to a hardware error.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type SetTime = extern "C" fn(&Time) -> Status;

/// Returns the current wakeup alarm clock setting.
///
/// # Parameters
///
/// * `Enabled` - Indicates if the alarm is currently enabled or disabled.
/// * `Pending` - Indicates if the alarm signal is pending and requires
///   acknowledgement.
/// * `Time` - The current alarm setting.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The alarm settings were returned.
/// * `EFI_INVALID_PARAMETER` - Enabled/Pending/Time is NULL.
/// * `EFI_DEVICE_ERROR` - The wakeup time could not be retrieved due to a
///   hardware error.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made. The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type GetWakeupTime = extern "C" fn(&mut bool, &mut bool, &mut Time) -> Status;

/// Sets the system wakeup alarm clock time.
///
/// # Parameters
///
/// * `Enable` - Enable or disable the wakeup alarm.
/// * `Time` - If Enable is TRUE, the time to set the wakeup alarm for. Type
///   EFI_TIME is defined in the GetTime() function description. If Enable is
///   FALSE, then this parameter is optional, and may be NULL.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - If Enable is TRUE, then the wakeup alarm was enabled. If
///   Enable is FALSE, then the wakeup alarm was disabled.
/// * `EFI_INVALID_PARAMETER` - A time field is out of range.
/// * `EFI_DEVICE_ERROR` - The wakeup time could not be set due to a hardware
///   error.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made. The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type SetWakeupTime = extern "C" fn(&bool, &Time) -> Status;

/// Changes the runtime addressing mode of EFI firmware from physical to
/// virtual.
///
/// # Parameters
///
/// * `MemoryMapSize` - The size in bytes of VirtualMap.
/// * `DescriptorSize` - The size in bytes of an entry in the VirtualMap.
/// * `DescriptorVersion` - The version of the structure entries in VirtualMap.
/// * `VirtualMap` - An array of memory descriptors which contain new virtual
///   address mapping information for all runtime ranges. Type
///   EFI_MEMORY_DESCRIPTOR is defined in the EFI_BOOT_SERVICES.GetMemoryMap()
///   function description.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The virtual address map has been applied.
/// * `EFI_UNSUPPORTED` - EFI firmware is not at runtime, or the EFI firmware is
///   already in virtual address mapped mode.
/// * `EFI_INVALID_PARAMETER` - DescriptorSize or DescriptorVersion is invalid.
/// * `EFI_NO_MAPPING` - A virtual address was not supplied for a range in the
///   memory map that requires a mapping.
/// * `EFI_NOT_FOUND` - A virtual address was supplied for an address that is
///   not found in the memory map.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type SetVirtualAddressMap = extern "C" fn(usize, usize, u32, &MemoryDescriptor) -> Status;

/// Determines the new virtual address that is to be used on subsequent memory
/// accesses.
///
/// # Parameters
///
/// * `DebugDisposition` - Supplies type information for the pointer being
///   converted.
/// * `Address` - A pointer to a pointer that is to be fixed to be the value
///   needed for the new virtual address mappings being applied.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The pointer pointed to by Address was modified.
/// * `EFI_NOT_FOUND` - The pointer pointed to by Address was not found to be
///   part of the current memory map. This is normally fatal.
/// * `EFI_INVALID_PARAMETER` - Address is NULL.
/// * `EFI_INVALID_PARAMETER` - *Address is NULL and DebugDisposition does not
///   have the EFI_OPTIONAL_PTR bit set.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type ConvertPointer = extern "C" fn(usize, &*mut core::ffi::c_void) -> Status;

/// Resets the entire platform. If the platform supports
/// EFI_RESET_NOTIFICATION_PROTOCOL, then prior to completing the reset of the
/// platform, all of the pending notifications must be called.
///
/// # Parameters
///
/// * `ResetType` - The type of reset to perform.
/// * `ResetStatus` - The status code for the reset. If the system reset is part
///   of a normal operation, the status code would be EFI_SUCCESS. If the system
///   reset is due to some type of failure the most appropriate EFI Status code
///   would be used.
/// * `DataSize` - The size, in bytes, of ResetData.
/// * `ResetData` - For a ResetType of EfiResetCold, EfiResetWarm, or
///   EfiResetShutdown the data buffer starts with a Null-terminated string,
///   optionally followed by additional binary data. The string is a description
///   that the caller may use to further indicate the reason for the system
///   reset. For a ResetType of EfiResetPlatformSpecific the data buffer also
///   starts with a Null-terminated string that is followed by an EFI_GUID that
///   describes the specific type of reset to perform.
pub type ResetSystem = extern "C" fn(ResetType, Status, usize, *const core::ffi::c_void);

// EFI_RESET_SYSTEM Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum ResetType {
    /// Calling this interface with ResetType of EfiResetCold causes a
    /// system-wide reset. This sets all circuitry within the system to its
    /// initial state. This type of reset is asynchronous to system operation
    /// and operates without regard to cycle boundaries. EfiResetCold is
    /// tantamount to a system power cycle.
    ResetCold,

    /// Calling this interface with ResetType of EfiResetWarm causes a
    /// system-wide initialization. The processors are set to their initial
    /// state, and pending cycles are not corrupted. If the system does not
    /// support this reset type, then an EfiResetCold must be performed.
    ResetWarm,

    /// Calling this interface with ResetType of EfiResetShutdown causes the
    /// system to enter a power state equivalent to the ACPI G2/S5 or G3 states.
    /// If the system does not support this reset type, then when the system is
    /// rebooted, it should exhibit the EfiResetCold attributes.
    ResetShutdown,

    /// Calling this interface with ResetType of EfiResetPlatformSpecific causes
    /// a system-wide reset. The exact type of the reset is defined by the
    /// EFI_GUID that follows the Null-terminated Unicode string passed into
    /// ResetData. If the platform does not recognize the EFI_GUID in ResetData
    /// the platform must pick a supported reset type to perform.The platform
    /// may optionally log the parameters from any non-normal reset that occurs.
    ResetPlatformSpecific,
}

/// Returns the next high 32 bits of the platform's monotonic counter.
///
/// # Parameters
///
/// * `HighCount` - Pointer to returned value.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The next high monotonic count was returned.
/// * `EFI_DEVICE_ERROR` - The device is not functioning properly.
/// * `EFI_INVALID_PARAMETER` - HighCount is NULL.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type GetNextHighMonotonicCount = extern "C" fn(&mut u32) -> Status;

/// Passes capsules to the firmware with both virtual and physical mapping.
/// Depending on the intended consumption, the firmware may process the capsule
/// immediately. If the payload should persist across a system reset, the reset
/// value returned from EFI_QueryCapsuleCapabilitiesmust be passed into
/// ResetSystem() and will cause the capsule to be processed by the firmware as
/// part of the reset process.
///
/// # Parameters
///
/// * `CapsuleHeaderArray` - Virtual pointer to an array of virtual pointers to
///   the capsules being passed into update capsule. Each capsules is assumed to
///   stored in contiguous virtual memory. The capsules in the
///   CapsuleHeaderArray must be the same capsules as the ScatterGatherList. The
///   CapsuleHeaderArray must have the capsules in the same order as the
///   ScatterGatherList.
/// * `CapsuleCount` - Number of pointers to EFI_CAPSULE_HEADER in
///   CapsuleHeaderArray.
/// * `ScatterGatherList` - Physical pointer to a set of
///   EFI_CAPSULE_BLOCK_DESCRIPTORthat describes the location in physical memory
///   of a set of capsules. See "Related Definitions" for an explanation of how
///   more than one capsule is passed via this interface. The capsules in the
///   ScatterGatherList must be in the same order as the CapsuleHeaderArray.
///   This parameter is only referenced if the capsules are defined to persist
///   across system reset.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - Valid capsule was passed. If
///   CAPSULE_FLAGS_PERSIST_ACROSS_RESET is not set, the capsule has been
///   successfully processed by the firmware.
/// * `EFI_INVALID_PARAMETER` - CapsuleSize, or an incompatible set of flags
///   were set in the capsule header.
/// * `EFI_INVALID_PARAMETER` - CapsuleCount is 0
/// * `EFI_DEVICE_ERROR` - The capsule update was started, but failed due to a
///   device error.
/// * `EFI_UNSUPPORTED` - The capsule type is not supported on this platform.
/// * `EFI_OUT_OF_RESOURCES` - When ExitBootServices() has been previously
///   called this error indicates the capsule is compatible with this platform
///   but is not capable of being submitted or processed in runtime. The caller
///   may resubmit the capsule prior to ExitBootServices().
/// * `EFI_OUT_OF_RESOURCES` - When ExitBootServices()has not been previously
///   called then this error indicates the capsule is compatible with this
///   platform but there are insufficient resources to process.
/// * `EFI_UNSUPPORTED` - This call is not supported by this platform at the
///   time the call is made.  The platform must correctly reflect this behavior
///   in the RuntimeServicesSupported variable.
pub type UpdateCapsule = extern "C" fn(&&CapsuleHeader, usize, PhysicalAddress) -> Status;

// EFI_UPDATE_CAPSULE Related Definitions

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct CapsuleHeader {
    /// A GUID that defines the contents of a capsule.
    pub capsule_guid: Guid,

    /// The size of the capsule header. This may be larger than the size of the
    /// EFI_CAPSULE_HEADER since CapsuleGuid may imply extended header entries.
    pub header_size: u32,

    /// Bit-mapped list describing the capsule attributes. The Flag values of
    /// 0x0000 – 0xFFFF are defined by CapsuleGuid. Flag values of 0x10000 –
    /// 0xFFFFFFFF are defined by this specification
    pub flags: u32,

    /// Size in bytes of the capsule (including capsule header).
    pub capsule_image_size: u32,
}

/// Returns if the capsule can be supported via UpdateCapsule().
///
/// # Parameters
///
/// * CapsuleHeaderArray - Virtual pointer to an array of virtual pointers to
///   the capsules being passed into update capsule. The capsules are assumed to
///   stored in contiguous virtual memory.
/// * CapsuleCount - Number of pointers to EFI_CAPSULE_HEADER in
///   CapsuleHeaderArray.
/// * MaximumCapsuleSize - On output the maximum size in bytes that
///   UpdateCapsule() can support as an argument to UpdateCapsule() via
///   CapsuleHeaderArray and ScatterGatherList.Undefined on input.
/// * ResetType - Returns the type of reset required for the capsule update.
///   Undefined on input.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - Valid answer returned.
/// * EFI_INVALID_PARAMETER - MaximumCapsuleSize is NULL.
/// * EFI_UNSUPPORTED - The capsule type is not supported on this platform, and
///   MaximumCapsuleSize and ResetType are undefined.
/// * EFI_OUT_OF_RESOURCES - When ExitBootServices() has been previously called
///   this error indicates the capsule is compatible with this platform but is
///   not capable of being submitted or processed in runtime. The caller may
///   resubmit the capsule prior to ExitBootServices().
/// * EFI_OUT_OF_RESOURCES - When ExitBootServices()has not been previously
///   called then this error indicates the capsule is compatible with this
///   platform but there are insufficient resources to process.
/// * EFI_UNSUPPORTED - This call is not supported by this platform at the time
///   the call is made.  The platform must correctly reflect this behavior in
///   the RuntimeServicesSupported variable.
pub type QueryCapsuleCapabilities =
    extern "C" fn(&&CapsuleHeader, usize, &mut u64, &mut ResetType) -> Status;

/// # Protocols - Device Path Protocol

/// Can be used on any device handle to obtain generic path/location information
/// concerning the physical device or logical device. If the handle does not
/// logically map to a physical device, the handle may not necessarily support
/// the device path protocol. The device path describes the location of the
/// device the handle is for. The size of the Device Path can be determined from
/// the structures that make up the Device Path.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct DevicePathProtocol {
    pub r#type: u8,
    pub sub_type: u8,
    pub length: [u8; 2],
}

/// # Protocols - Console Support

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct KeyToggleState(pub u8);

/// The Simple Text Input protocol defines the minimum input required to support
/// the ConsoleIn device. The EFI specification requires that the
/// EFI_SIMPLE_TEXT_INPUT_PROTOCOL supports the same languages as the
/// corresponding EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct SimpleTextInputProtocol {
    /// Reset the ConsoleIn device.
    pub reset: InputReset,

    /// Returns the next input character.
    pub read_key_stroke: InputReadKey,

    /// Event to use with EFI_BOOT_SERVICES.WaitForEvent() to wait for a key to
    /// be available.
    pub wait_for_key: Event,
}

/// Resets the input device hardware.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_INPUT_PROTOCOL instance.
/// * `ExtendedVerification` - Indicates that the driver may perform a more
///   exhaustive verification operation of the device during reset.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The device was reset.
/// * `EFI_DEVICE_ERROR` - The device is not functioning correctly and could not
///   be reset.
pub type InputReset = extern "C" fn(&SimpleTextInputProtocol, bool) -> Status;

/// Reads the next keystroke from the input device.
///
/// # Parameters
///
/// * This - A pointer to the EFI_SIMPLE_TEXT_INPUT_PROTOCOL instance.
/// * Key - A pointer to a buffer that is filled in with the keystroke
///   information for the key that was pressed.
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The keystroke information was returned.
/// * EFI_NOT_READY - There was no keystroke data available.
/// * EFI_DEVICE_ERROR - The keystroke information was not returned due to
///   hardware errors.
pub type InputReadKey = extern "C" fn(&SimpleTextInputProtocol, &mut InputKey) -> Status;

// EFI_INPUT_READ_KEY Related Definitions

/// A buffer that is filled in with the keystroke information for the key that
/// was pressed.
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct InputKey {
    pub scan_code: u16,
    pub unicode_char: u16,
}

/// The Simple Text Output protocol defines the minimum requirements for a
/// text-based ConsoleOut device. The EFI specification requires that the
/// EFI_SIMPLE_TEXT_INPUT_PROTOCOLsupport the same languages as the
/// corresponding EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct SimpleTextOutputProtocol<'a> {
    /// Reset the ConsoleOut device.
    pub reset: TextReset,

    /// Displays the string on the device at the current cursor location.
    pub output_string: TextString,

    /// Tests to see if the ConsoleOut device supports this string.
    pub test_string: TextTestString,

    /// Queries information concerning the output device’s supported text mode.
    pub query_mode: TextQueryMode,

    /// Sets the current mode of the output device.
    pub set_mode: TextSetMode,

    /// Sets the foreground and background color of the text that is output.
    pub set_attribute: TextSetAttribute,

    /// Clears the screen with the currently set background color.
    pub clear_screen: TextClearScreen,

    /// Sets the current cursor position.
    pub set_cursor_position: TextSetCursorPosition,

    /// Turns the visibility of the cursor on/off.
    pub enable_cursor: TextEnableCursor,

    /// Pointer to SIMPLE_TEXT_OUTPUT_MODE data.
    pub mode: &'a SimpleTextOutputMode,
}

// EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL Related Definitions

/// The SIMPLE_TEXT_OUTPUT protocol is used to control text-based output
/// devices. It is the minimum required protocol for any handle supplied as the
/// ConsoleOut or StandardError device. In addition, the minimum supported text
/// mode of such devices is at least 80 x 25 characters.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct SimpleTextOutputMode {
    /// The number of modes supported by QueryMode() and SetMode().
    pub max_mode: i32,

    /// The text mode of the output device(s).
    pub mode: i32,

    /// The current character output attribute.
    pub attribute: i32,

    /// The cursor's column.
    pub cursor_column: i32,

    /// The cursor's row.
    pub cursor_row: i32,

    /// The cursor is currently visible or not.
    pub cursor_visible: bool,
}

/// Resets the text output device hardware.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `ExtendedVerification` - Indicates that the driver may perform a more
///   exhaustive verification operation of the device during reset.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The text output device was reset.
/// * `EFI_DEVICE_ERROR` - The text output device is not functioning correctly
///   and could not be reset.
pub type TextReset = extern "C" fn(&SimpleTextOutputProtocol<'_>, bool)
    -> Status;

/// Writes a string to the output device.
///
/// # EFI Cursor Location/Advance Rules
///
/// | Mnemonic | Unicode | Description                                         |
/// | -------- | ------- | --------------------------------------------------- |
/// | Null     | U+0000  | Ignore the character, and do not move the cursor.   |
/// | BS       | U+0008  | If the cursor is not at the left edge of the
///                        display, then move the cursor left one column.      |
/// | LF       | U+000A  | If the cursor is at the bottom of the display, then
///                        scroll the display one row, and do not update the
///                        cursor position. Otherwise, move the cursor down one
///                        row.                                                |
/// | CR       | U+000D  | Move the cursor to the beginning of the current row.|
/// | Other    | U+XXXX  | Print the character at the current cursor position
///                        and move the cursor right one column. If this moves
///                        the cursor past the right edge of the display, then
///                        the line should wrap to the beginning of the next
///                        line. This is equivalent to inserting a CR and an
///                        LF. Note that if the cursor is at the bottom of the
///                        display, and the line wraps, then the display will
///                        be scrolled one line.                               |
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `String` - The Null-terminated string to be displayed on the output
///   device(s).
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The string was output to the device.
/// * `EFI_DEVICE_ERROR` - The device reported an error while attempting to
///   output the text.
/// * `EFI_UNSUPPORTED` - The output device’s mode is not currently in a defined
///   text mode.
/// * `EFI_WARN_UNKNOWN_GLYPH` - This warning code indicates that some of the
///   characters in the string could not be rendered and were skipped.
pub type TextString = extern "C" fn(&SimpleTextOutputProtocol<'_>, &[u16])
    -> Status;

/// Verifies that all characters in a string can be output to the target device.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `String` - The Null-terminated string to be examined for the output
///   device(s).
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The device(s) are capable of rendering the output string.
/// * `EFI_UNSUPPORTED` - Some of the characters in the string cannot be
///   rendered by one or more of the output devices mapped by the EFI handle.
pub type TextTestString = extern "C" fn(&SimpleTextOutputProtocol<'_>, &u16)
    -> Status;

/// Returns information for an available text mode that the output device(s)
/// supports.
///
/// # Parameters
///
/// * `This` - A pointer to theEFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `ModeNumber` - The mode number to return information on.
/// * `Columns` - Returns the geometry of the text output device for the request
///   ModeNumber.
/// * `Rows` - Returns the geometry of the text output device for the request
///   ModeNumber.
///
/// # Status Code Returned
///
/// * `EFI_SUCCESS` - The requested mode information was returned.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request.
/// * `EFI_UNSUPPORTED` - The mode number was not valid.
pub type TextQueryMode =
    extern "C" fn(&SimpleTextOutputProtocol<'_>, usize, &mut usize, &mut usize)
        -> Status;

/// Sets the output device(s) to a specified mode.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `ModeNumber` - The text mode to set.
///
/// # Status Code Returned
///
/// * `EFI_SUCCESS` - The requested text mode was set.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request.
/// * `EFI_UNSUPPORTED` - The mode number was not valid.
pub type TextSetMode = extern "C" fn(&SimpleTextOutputProtocol<'_>, usize)
    -> Status;

/// Sets the background and foreground colors for the OutputString() and
/// ClearScreen() functions.
///
/// # Parameters
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `Attribute` - The attribute to set. Bits 0..3 are the foreground color,
///   and bits 4..6 are the background color. All other bits are reserved.
///
/// # Status Code Returned
///
/// * `EFI_SUCCESS` - The requested attributes were set.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request.
pub type TextSetAttribute = extern "C" fn(&SimpleTextOutputProtocol<'_>, usize)
    -> Status;

/// Clears the output device(s) display to the currently selected background
/// color.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The operation completed successfully.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request.
/// * `EFI_UNSUPPORTED` - The output device is not in a valid text mode.
pub type TextClearScreen = extern "C" fn(&SimpleTextOutputProtocol<'_>)
    -> Status;

/// Sets the current coordinates of the cursor position.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `Column` - The position to set the cursor to. Must greater than or equal
///   to zero and less than the number of columns returned by QueryMode().
/// * `Row` - The position to set the cursor to. Must greater than or equal to
///   zero and less than the number of rows returned by QueryMode().
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The operation completed successfully.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request.
/// * `EFI_UNSUPPORTED` - The output device is not in a valid text mode, or the
///   cursor position is invalid for the current mode.
pub type TextSetCursorPosition = extern "C" fn(&SimpleTextOutputProtocol<'_>,
                                               usize, usize) -> Status;

/// Makes the cursor visible or invisible.
///
/// # Parameters
///
/// * `This` - A pointer to the EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL instance.
/// * `Visible` - If TRUE, the cursor is set to be visible. If FALSE, the cursor
///   is set to be invisible.
///
/// # Status Codes Returned
///
/// * `EFI_SUCCESS` - The operation completed successfully.
/// * `EFI_DEVICE_ERROR` - The device had an error and could not complete the
///   request or the device does not support changing the cursor mode.
/// * `EFI_UNSUPPORTED` - The output device does not support visibility control
///   of the cursor.
pub type TextEnableCursor = extern "C" fn(&SimpleTextOutputProtocol<'_>, bool)
    -> Status;

/// # Status Codes

/// Status Codes
///
/// The range of status codes that have the highest bit set and the next to
/// highest bit clear are reserved for use by EFI. The range of status codes
/// that have both the highest bit set and the next to highest bit set are
/// reserved for use by OEMs. Success and warning codes have their highest bit
/// clear, so all success and warning codes have positive values. The range of
/// status codes that have both the highest bit clear and the next to highest
/// bit clear are reserved for use by EFI. The range of status code that have
/// the highest bit clear and the next to highest bit set are reserved for use
/// by OEMs.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(transparent)]
pub struct Status(pub usize);

impl Status {
    /// The operation was aborted.
    pub const ABORTED: Status = Status(21 | Status::HIGH_BIT);
    /// Access was denied.
    pub const ACCESS_DENIED: Status = Status(15 | Status::HIGH_BIT);
    /// The protocol has already been started.
    pub const ALREADY_STARTED: Status = Status(20 | Status::HIGH_BIT);
    /// The buffer was not the proper size for the request.
    pub const BAD_BUFFER_SIZE: Status = Status(4 | Status::HIGH_BIT);
    /// The buffer is not large enough to hold the requested data. The required
    /// buffer size is returned in the appropriate parameter when this error
    /// occurs.
    pub const BUFFER_TOO_SMALL: Status = Status(5 | Status::HIGH_BIT);
    /// The security status of the data is unknown or compromised and the data
    /// must be updated or replaced to restore a valid security status.
    pub const COMPROMISED_DATA: Status = Status(33 | Status::HIGH_BIT);
    /// A CRC error was detected.
    pub const CRC_ERROR: Status = Status(27 | Status::HIGH_BIT);
    /// The physical device reported an error while attempting the operation.
    pub const DEVICE_ERROR: Status = Status(7 | Status::HIGH_BIT);
    /// The resulting buffer was too small, and the data was truncated to the
    /// buffer size.
    pub const EFI_WARN_BUFFER_TOO_SMALL: Status = Status(4);
    /// The handle was closed, but the file was not deleted.
    pub const EFI_WARN_DELETE_FAILURE: Status = Status(2);
    /// The resulting buffer contains UEFI-compliant file system.
    pub const EFI_WARN_FILE_SYSTEM: Status = Status(6);
    /// The operation will be processed across a system reset.
    pub const EFI_WARN_RESET_REQUIRED: Status = Status(7);
    /// The data has not been updated within the timeframe set by localpolicy
    /// for this type of data.
    pub const EFI_WARN_STALE_DATA: Status = Status(5);
    // EFI_STATUS Warning Codes (High Bit Clear)

    /// The string contained one or more characters that the device could not
    /// render and were skipped.
    pub const EFI_WARN_UNKNOWN_GLYPH: Status = Status(1);
    /// The handle was closed, but the data to the file was not flushedproperly.
    pub const EFI_WARN_WRITE_FAILURE: Status = Status(3);
    /// The end of the file was reached.
    pub const END_OF_FILE: Status = Status(31 | Status::HIGH_BIT);
    /// Beginning or end of media was reached
    pub const END_OF_MEDIA: Status = Status(28 | Status::HIGH_BIT);
    const HIGH_BIT: usize = 1 << (core::mem::size_of::<usize>() * 8 - 1);
    /// A HTTP error occurred during the network operation.
    pub const HTTP_ERROR: Status = Status(35 | Status::HIGH_BIT);
    /// An ICMP error occurred during the network operation.
    pub const ICMP_ERROR: Status = Status(22 | Status::HIGH_BIT);
    /// The function encountered an internal version that was incompatible with
    /// a version requested by the caller.
    pub const INCOMPATIBLE_VERSION: Status = Status(25 | Status::HIGH_BIT);
    /// The language specified was invalid.
    pub const INVALID_LANGUAGE: Status = Status(32 | Status::HIGH_BIT);
    /// A parameter was incorrect.
    pub const INVALID_PARAMETER: Status = Status(2 | Status::HIGH_BIT);
    /// There is an address conflict address allocation
    pub const IP_ADDRESS_CONFLICT: Status = Status(34 | Status::HIGH_BIT);
    // EFI_STATUS Error Codes (High Bit Set)

    /// The image failed to load.
    pub const LOAD_ERROR: Status = Status(1 | Status::HIGH_BIT);
    /// The medium in the device has changed since the last access.
    pub const MEDIA_CHANGED: Status = Status(13 | Status::HIGH_BIT);
    /// The item was not found.
    pub const NOT_FOUND: Status = Status(14 | Status::HIGH_BIT);
    /// There is no data pending upon return.
    pub const NOT_READY: Status = Status(6 | Status::HIGH_BIT);
    /// The protocol has not been started.
    pub const NOT_STARTED: Status = Status(19 | Status::HIGH_BIT);
    /// A mapping to a device does not exist.
    pub const NO_MAPPING: Status = Status(17 | Status::HIGH_BIT);
    /// The device does not contain any medium to perform the operation.
    pub const NO_MEDIA: Status = Status(12 | Status::HIGH_BIT);
    /// The server was not found or did not respond to the request.
    pub const NO_RESPONSE: Status = Status(16 | Status::HIGH_BIT);
    /// A resource has run out.
    pub const OUT_OF_RESOURCES: Status = Status(9 | Status::HIGH_BIT);
    /// A protocol error occurred during the network operation.
    pub const PROTOCOL_ERROR: Status = Status(24 | Status::HIGH_BIT);
    /// The function was not performed due to a security violation.
    pub const SECURITY_VIOLATION: Status = Status(26 | Status::HIGH_BIT);
    // EFI_STATUS Success Codes (High Bit Clear)

    /// The operation completed successfully.
    pub const SUCCESS: Status = Status(0);
    /// A TFTP error occurred during the network operation.
    pub const TFTP_ERROR: Status = Status(23 | Status::HIGH_BIT);
    /// The timeout time expired.
    pub const TIMEOUT: Status = Status(18 | Status::HIGH_BIT);
    /// The operation is not supported.
    pub const UNSUPPORTED: Status = Status(3 | Status::HIGH_BIT);
    /// An inconstancy was detected on the file system causing the operating to
    /// fail.
    pub const VOLUME_CORRUPTED: Status = Status(10 | Status::HIGH_BIT);
    /// There is no more space on the file system.
    pub const VOLUME_FULL: Status = Status(11 | Status::HIGH_BIT);
    /// The device cannot be written to.
    pub const WRITE_PROTECTED: Status = Status(8 | Status::HIGH_BIT);
}
